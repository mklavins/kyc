﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.Client.Models
{
    public class UserInfoViewModel
    {
        // Is system administrator
        public bool IsAdmin { get; set; }
    }
}