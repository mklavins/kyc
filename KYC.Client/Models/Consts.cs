﻿using System.Configuration;

namespace KYC.Client.Models
{
    // Configuration of system base URLs. Set values in Web.config section <appSettings>.
    public static class BaseAddress
    {
        // System base URL
        public static readonly string Base = ConfigurationManager.AppSettings["BaseAddress"];
        // API URL
        public static readonly string API = Base + ConfigurationManager.AppSettings["ApiPath"];
        // Client app URL
        public static readonly string Client = Base + ConfigurationManager.AppSettings["ClientPath"];
    }
}