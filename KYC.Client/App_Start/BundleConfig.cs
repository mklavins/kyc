﻿using System.Web;
using System.Web.Optimization;

namespace KYC.Client
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.min.js",
                        "~/Scripts/tether.min.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/Scripts/DataTables/jquery.dataTables.min.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.min.js",
                        "~/Scripts/DataTables/dataTables.responsive.min.js",
                        "~/Scripts/DataTables/responsive.bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/jquery-ui.min.css",
                      "~/Content/DataTables/css/datatables.bootstrap.min.css",
                      "~/Content/DataTables/css/responsive.bootstrap.min.css",
                      "~/Content/site.css"
                      ));

            /// Scripts for views -----------------

            // SignIn view
            bundles.Add(new ScriptBundle("~/bundles/Authentication").Include(
                      "~/Scripts/Helpers/SignOut.js",
                      "~/Scripts/Views/Authentication.js"));

            // Main layout
            bundles.Add(new ScriptBundle("~/bundles/SignedIn").Include(
                      "~/Scripts/Helpers/SignOut.js",
                      "~/Scripts/Views/SignedIn.js"));

            // Edit User Profile view
            bundles.Add(new ScriptBundle("~/bundles/User/Edit").Include(
                      "~/Scripts/Views/User/Edit.js",
                      "~/Scripts/Helpers/GetModelErrors.js",
                      "~/Scripts/Helpers/StatusBar.js",
                      "~/Scripts/Helpers/IsChecked.js"));

              // View User Profile view
              bundles.Add(new ScriptBundle("~/bundles/User/View").Include(
                      "~/Scripts/Views/User/View.js"));

            // User Profile list view
            bundles.Add(new ScriptBundle("~/bundles/User/List").Include(
                      "~/Scripts/Views/User/List.js"));

            // Company list view
            bundles.Add(new ScriptBundle("~/bundles/Company/List").Include(
                      "~/Scripts/Views/Company/List.js"));

            // Add Company view
            bundles.Add(new ScriptBundle("~/bundles/Company/Add").Include(
                      "~/Scripts/Views/Company/Add.js",
                      "~/Scripts/Helpers/GetModelErrors.js",
                      "~/Scripts/Helpers/IsChecked.js"));

            // Edit Company view
            bundles.Add(new ScriptBundle("~/bundles/Company/Edit").Include(
                      "~/Scripts/Views/Company/Edit.js",
                      "~/Scripts/Views/Company/Quiz.js",
                      "~/Scripts/Helpers/GetModelErrors.js",
                      "~/Scripts/Helpers/IsChecked.js"));

            // Fill company questionnaire view
            bundles.Add(new ScriptBundle("~/bundles/Quiz/Company").Include(
                      "~/Scripts/Views/Quiz/Company.js"));

            // Fill company quiz view
            bundles.Add(new ScriptBundle("~/bundles/Quiz").Include(
                    "~/Scripts/Views/Quiz/Index.js",
                    "~/Scripts/Helpers/StatusBar.js"));
        }
    }
}
