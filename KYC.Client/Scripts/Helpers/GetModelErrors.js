﻿// Extracts error message texts from api validation error message.
function getModelErrors(error) {
    var response = error.responseJSON.modelState;

    var responseMessage = ' ';

    $.each(response, function (key, message) {
        responseMessage += message + ' ';
    });

    return responseMessage;
}