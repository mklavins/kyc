﻿/* See system function requirements - function S4.USER.PROFILE.STATS - for more information
 * Used in Quiz and User/Profile/Edit views */

var CorrectAnswers = 0;
var TotalAnswers = 0

// Get quiz stats
// dataAbput: 
// 'User' - user correct answers vs. total answers;
// 'Colleague' - colleagues coorect answers about active user vs. total answers
// progressBar: id of progress bar
function LoadStatusBar(progressBar, dataAbout) {
    $.ajax({
        type: "GET",
        url: apiBaseAddress + 'Quiz/Stats/'+dataAbout,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            // If there are no answers
            if (data.totalCount == 0) {
                $(progressBar).hide();
            }
            else {
                // update inner vars and visualize answers in bar
                CorrectAnswers = data.correctCount;
                TotalAnswers = data.totalCount;
                updateStatusBar(progressBar);
            }
        }
    });
}

// Visualize answer counts int progress bar
// progressBar: id of progress bar
function updateStatusBar(progressBar) {
    if (TotalAnswers > 0) {
        var correct = (CorrectAnswers / TotalAnswers * 100).toFixed(2);
        var incorrect = (100 - correct).toFixed(2);
        $(progressBar + '-correct').width(correct + '%').show();
        $(progressBar + '-wrong').width(incorrect + '%').show();
        $(progressBar).show('slide');
    }
}