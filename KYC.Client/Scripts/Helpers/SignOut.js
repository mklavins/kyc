﻿// Sign out of system
function signOut(statusCode) {
    localStorage.clear();
    window.location.replace(clientBaseAddress + 'SignOut?status_code=' + statusCode);
}