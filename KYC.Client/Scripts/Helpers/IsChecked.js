﻿// Returns true if checkbox element is checked, false otherwise
function isChecked(element) {
    return element.is(":checked");
}