﻿/* See system function requirements - function S1.USERS.PROFILE.AUTHENTICATE and S1.USERS.PROFILE.SIGNOUT - for more information
 * Used in every view while user is authenticated */

$(document).ready(function () {

    // Close message box
    $('#linkClose').click(function () {
        hideMessage();
    });
});

// Shows info message on top of Layout page.
// Type (success, info, warning, danger) represents message box style
function showMessage(type, messageText) {
    // if there already is message box- hide it
    hideMessage();
    // show new message
    $('#message').removeClass();
    $('#messageText').text(messageText);
    $('#message').addClass('alert alert-' + type);
    $('#message').show('drop', { direction: 'down' }, 200);

    // hide message automatically after 5 seconds
    setTimeout(function () {
        hideMessage();
    }, 5000);
}

// Hide error message, restore its default style 
function hideMessage() {
    $('#message').hide('drop', {direction:'down'}, 200, function () {
        $('#messageText').text();
    });
}

// Check if user is signed in
function signedIn() {
    // if user is not signed in, redirect to SignIn page
    if (localStorage.getItem('accessToken') === null || localStorage.getItem('userName') === null) {
        signOut(401);
    }
    // otherwise check for User profile notifications
    else {
        $(document).ready(function () {
            var userNameLbl = $('#signedIn').find('#userName');
            userNameLbl.text(localStorage.getItem('userName'));
            GetProfileStatus();
        });
    }
}

// Get user profile notifications
function GetProfileStatus() {
    var notificationCount = 0;
    $.ajax({
        url: apiBaseAddress + 'Account/Status',
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            // chech, if user is accepted his profile info, if not - redirect to Profile page
            if (!data.profileConfirmed && window.location.href !== clientBaseAddress + 'User/Profile/Edit') {
                window.location.replace(clientBaseAddress + 'User/Profile/Edit');
            }
            // get and show count of profile notifications badge near user name
            else {
                // Count notifications
                $.each(data, function (index, value) {
                    if (!value) {
                        notificationCount++;
                    }
                });
                // If there are any notifications. show badge 
                if (notificationCount > 0) {
                    $('#notificationBadge').removeClass('hidden');
                    $('#notificationBadge').text(notificationCount);
                }
            }
        },
        error: function (jqXHR) {
            // User is not authenticated
            signOut(jqXHR.status);
        }
    });
}