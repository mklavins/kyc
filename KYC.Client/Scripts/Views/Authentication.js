﻿/* See system function requirements - function S1.USERS.PROFILE.AUTHENTICATE - for more information
 * Used in SignIn/Index view */

// Extracts accessToken from URI
function getAccessToken() {
    if (location.hash) {
        if (location.hash.split('access_token=')) {
            var accessToken = location.hash.split('access_token=')[1].split('&')[0];
            if (accessToken) {
                // if user token is present in uri, check if user is registered in system
                isUserRegistered(accessToken);
            }
        }
    }
    // if accessToken is not present, show Sign in window
    else {
        $("#main").ready(function () {
            $("#main").fadeIn(500);
        });
    }
}
// Authenticate user in system, if user is registered, otherwise redirect to registration
function isUserRegistered(accessToken) 
{
    // check if user is registered in system
    $.ajax({
        url: apiBaseAddress + 'Account/UserInfo',
        method: 'GET',
        headers: {
            'content-type': 'application/JSON',
            'Authorization': 'Bearer ' + accessToken
        },
        success: function (response) {
            // user is registered - signing in
            if (response.hasRegistered) {
                localStorage.setItem('accessToken', accessToken);
                localStorage.setItem('userName', response.name);
                signIn(response);
            }
            // user is not registered - redirect to registration
            else {
                signupExternalUser(accessToken);
            }
        }
    });
}

// Register user in system and redirect to beginning of authentication process
function signupExternalUser(accessToken) 
{
    $.ajax({
        url: apiBaseAddress + 'Account/RegisterExternal',
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + accessToken
        },
        // user is registered - redirect to beginning of authentication process
        success: function () {
            var redirect_uri = encodeURI(clientBaseAddress + 'SignIn');
            window.location.replace(apiBaseAddress + 'Account/ExternalLogin?provider=Google&response_type=token&client_id=self&redirect_uri=' + redirect_uri + '&state=-tA5EkceFw4AdKM3yyJqBBOQQCI5K3yK5R0Lfm9XHGc1');
        },
        // user is not registered - return to Sign in page and show error message.
        error: function (err) {
            localStorage.clear();
            signOut(err.status);
        }
    });
}

// Saves user access info on app server session.
function signIn(user) {
    $.ajax({
        url: clientBaseAddress + 'SignIn/SignedIn',
        method: 'POST',
        headers: {
            'content-type': 'application/JSON',
        },
        data: JSON.stringify({
            isAdmin: user.isAdmin,
        }),
        success: function () {
            window.location.replace(clientBaseAddress + 'Colleagues');
        },
        error: function (err) {
            window.location.replace(clientBaseAddress + '?status_code=' + err.status);
        }
    });
}