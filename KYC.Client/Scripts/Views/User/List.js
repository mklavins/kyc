﻿/* See system function requirements - function S1.USERS.PROFILE.LIST - for more information
 * File includes Datatables v.1.10.12 plugin
 * Used in Users/UsersList view */

$.fn.dataTable.ext.errMode = 'none';    // Disables datatables eror alert window

$('#colleagueTable').on('error.dt', function (e, settings, techNote, message) {
    console.log('An error has been reported: ', message);
}).DataTable({      // initialize data tables plugin
    responsive: true,
    ajax: {
        url: apiBaseAddress + 'Colleagues',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        dataSrc: ''
    },
    columns: [
        {
            data: 'name',
            render: function (data, type, user) {
                return '<a href="' + clientBaseAddress + 'User/Profile/View/' + user.userId + '">' + data + '</a>';
            }
        },
        { data: 'email' },
        { data: 'phone' }
    ],
    'bInfo': false,         // disable "showing" text (from which to which number element is included in table)
    'pageLength': 20,       // row number in one page
    'lengthChange': false,  // disable changing the row count in page
    'language':{            // override message text
        'emptyTable': 'You don’t have any colleagues registered in system.',
        "zeroRecords": "No colleagues were found!"
    }
});