﻿/* See system function requirements - functions S1.USERS.PROFILE.EDIT and S4.USER.PROFILE.STATS - for more information
 * File includes datapicker jQuery-ui plugin
 * Used in Users/EditUserProfile view */

$(document).ready(function () {

    LoadData();

    LoadStatusBar('#progressUser', 'User');
    LoadStatusBar('#progressColleague', 'Colleagues');
    AreQuestionsAnswered();

    // Initializes datapicker jQuery-ui plugin
    $('#dobTxt').datepicker({
        dateFormat: 'yy/mm/dd',
        changeMonth: true,
        changeYear: true,
        showOn: 'focus'
    });

    // When Save changes button is pressed
    $('#saveBtn').click(function () {
        SaveData();
    });

    // When Discard changes button is pressed
    $('#discardBtn').click(function () {
        LoadData();
    });
});

$(document).ajaxStop(function () {
    // Depending on user activity, answer progress bars are shown or hidden
    if ($('#progressUser').is(':visible')) {
        $('#progressUserText').show();
    }
    if ($('#progressColleague').is(':visible')) {
        $('#progressColleagueText').show();
    }
    if (!$('#progressUser').is(':visible') && !$('#progressColleague').is(':visible')) {
        $('#stats').hide();
    }
});

// Get user profile data from API 
function LoadData() {
    $.ajax({
        url: apiBaseAddress + 'Account/Edit',
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            $('#nameTxt').text(data.name);
            $('#emailTxt').text(data.email);
            $('#profileImg').attr("src", data.imageUrl + '?sz=150');
            $('#phoneTxt').val(data.phone);

            $('#dobTxt').val(data.dateOfBirth);

            $('#includeDobChk').prop('checked', data.includeDateOfBirth);

            // If user Google+ profile link is unavailable, hide table row
            if (data.googlePlusProfileUrl === null){
                $('#googleTxt').hide();
                $('#showGoogle').hide();
            }
            else {
                $('#googleTxt').attr('href', data.googlePlusProfileUrl);
                $('#includeGoogleChk').prop('checked', data.includeGooglePlusProfileUrl);
            }

            if (data.profileConfirmed === false)
                $('#notConfirmedDiv').show();

            if (data.isCompanyAdmin) {
                $('#companyAdminDiv a').attr('href', clientBaseAddress + 'Company/Edit/' + data.companyId);
                $('#companyAdminDiv').removeClass('collapse');
            }
        }
    });
}

// Updates user profile information
function SaveData() {
    $.ajax({
        type: "PUT",
        url: apiBaseAddress + 'Account/Edit',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({
            dateOfBirth: $('#dobTxt').val(),

            includeDateOfBirth: isChecked($('#includeDobChk')),
            phone: $('#phoneTxt').val(),
            includeGooglePlusProfileUrl: isChecked($('#includeGoogleChk')),
            profileConfirmed: true
        }),
        success: function (data) { showMessage('success', 'Profile data saved successfully!'); },
        error: function (error) {
            showMessage('danger', 'Profile data were not saved!' + getModelErrors(error));
        }
    });
}

// Check, whether user has answered to any questions from questionnaire
function AreQuestionsAnswered() {
    var notificationCount = 0;
    $.ajax({
        url: apiBaseAddress + 'Account/Status',
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            if (!data.questionsAnswered) {
                $('#statsErrorDiv').removeClass('collapse');
            }
        }
    });
}