﻿/* See system function requirements - function S1.USERS.PROFILE.VIEW - for more information
 * Used in Users/ViewUserProfile view */

// Get user data from API
function LoadData(userId) {
    $.ajax({
        url: apiBaseAddress + 'Colleagues/' + userId,
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            $('#nameTxt').text(data.name);
            $('#emailTxt').text(data.email);
            $('#profileImg').attr("src", data.imageUrl + '?sz=200');    // ?sz parameter describes profile image size in px
            $('#phoneTxt').text(data.phone);

            // adds date of birth if profile user has allowed it
            if (data.dateOfBirth !== null) {
                $('#main-table tbody').append('<tr>' +
                                              '<td class="text-left col-sm-2 col-lg-2"><label>Date of birth: </label></td>' +
                                              '<td class="col-sm-10 col-lg-10"><label>' + data.dateOfBirth + '</label></td>' +
                                              '</tr>');
            }
            // adds Google+ link if profile user has allowed it
            if (data.googlePlusProfileUrl !== null){
                $('#main-table tbody').append('<tr>'+
                                              '<td class="text-left col-sm-2 col-lg-2"><label>Google+ profile: </label></td>'+
                                              '<td class="col-sm-10 col-lg-10"><a href="'+data.googlePlusProfileUrl+'" target="_blank"><label">Go to profile</label></a></td>'+
                                              '</tr>');
            }
        },
        error: function (error) {
            // Redirects to colleague list view
            window.location.replace(clientBaseAddress + 'Colleagues?status_code=' + error.status);
        }
    });
}