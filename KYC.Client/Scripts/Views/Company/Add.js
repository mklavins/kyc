﻿/* See system function requirements - function S2.COMPANY.ADD - for more information
 * Used in Company/Add view */

// Save company data through API
function addCompany() {
    $.ajax({
        type: "POST",
        url: apiBaseAddress + 'Companies',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({
            name: $('#nameTxt').val(),
            address: $('#addressTxt').val(),
            email: $('#emailTxt').val(),
            phone: $('#phoneTxt').val()
        }),
        success: function (company) {
            // Redirect to company editing view S2.COMPANY.EDIT
            window.location.replace(clientBaseAddress + 'Company/Edit/' + company.companyId + '?status_code=201');
        },
        error: function (error) {
            // Show error
            showMessage('danger', 'Company was not added!' + getModelErrors(error));
        }
    });
}