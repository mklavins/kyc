﻿/* See system function requirements - function S3.QUIZ.QUESTION.LIST - for more information
 * Used in Company/Edot view as partial view */

$(document).ready(function () {
    // When open modal dialog for question or choice adding
    $('#dialogModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var questionId = button.data('qid');
        var modal = $(this);
        if (questionId != null) {
            var questionText = $('#qText' + questionId).text();
            modal.find('.modal-title').text('Add new choice');
            modal.find('.modal-body label').text(questionText);
            $('#modalQId').val(questionId);
        }
    });

    // When close modal dialog for question or choice adding
    $('#dialogModal').on('hidden.bs.modal', function (event) {
        var modal = $(this);
        modal.find('#modalText').val('');
        modal.find('.modal-title').text('Add question');
        modal.find('.modal-body label').text('');
        $('#addModalBtn').prop('disabled', false);
        $('#addModalBtn').text('Add');
        $('#modalQId').val('');
    });

    // When question/choice adding button is pressed
    $('#addModalBtn').click(function () {
        var $btn = $(this);
        var questionId = $('#modalQId').val();
        $btn.prop('disabled', true);
        $btn.text('Saving');
        if (questionId == '')
            AddQuestion();
        else {
            AddChoice(questionId);
        }
        $('#dialogModal').modal('hide');
    });

    // When question visibilidy checkbox is pressed
    $('#accordion').on('click', 'input[type="checkbox"]', function () {
        setQuestionVisibility($(this).val(), this.checked);
    });

    // When question deleting button is pressed
    $('#deleteQuestionModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        $('#deleteQuestionModalBtn').data('qid', button.data('qid'));
    });

    // When question deleting accept button is pressed
    $('#deleteQuestionModalBtn').click(function () {
        deleteQuestion($(this).data('qid'));
    });
});

// Get all company questions and choices from API
function GetQuiz(id) {
    $.ajax({
        type: "GET",
        url: apiBaseAddress + 'Questions/?companyId='+id,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            unloadQuiz();
            if (data !== null) {
                $.each(data, function (index, question) {
                    showQuestion(question);
                });
            }
            else {
                showMessage('info', 'There are no questions added for this company.');
            }
            // Show Add question button
            $('#addBtnDiv').append('<button type="button" class="btn btn-success pull-right"' +
                            'data-toggle="modal" data-target="#dialogModal" id="addQuestionBtn">Add question</button>');
        },
        error: function (error) {
            // User is not company admin
            if (error.status === 403) {
                $('#notAdminDiv').show();
            }
            unloadQuiz();
        }
    });
}

// Add new question through API and add to question list
function AddQuestion() {
    var $btn = $('#addModalBtn');   // Add button in modal dialog
    $btn.prop('disabled', true); // while question / choice adding request is being processed, disable Add button in modal dialog
    $btn.text('Saving');
    $.ajax({
        type: 'POST',
        url: apiBaseAddress + 'Questions',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({ text: $('#modalText').val() }),
        success: function (data) {
            showQuestion(data);
            showMessage('success', 'Question created successfully!');
        },
        error: function (error) {
            if (error.status === 403) {
                document.location.href = clientBaseAddress + 'Colleagues';
            }
            showMessage('danger', 'Question was not created! ' + getModelErrors(error));
        }
    });
}

// Add new choice for question and show it in the question list
function AddChoice(questionId) {
    $.ajax({
        type: 'POST',
        url: apiBaseAddress + 'Questions/'+ questionId + '/Choices',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({
            text: $('#modalText').val(),
            questionId: questionId
        }),
        success: function (data) {
            showChoice(data);
            showMessage('success', 'Choice created successfully!');
        },
        error: function (error) {
            if (error.status === 403) {
                document.location.href = clientBaseAddress + 'Colleagues';
            }
            showMessage('danger', 'Choice was not created! ' + getModelErrors(error));
        }
    });
}

// Add question and its choices to question list
function showQuestion(data) {
    var id = data.questionId;
    $('#accordion').append('<div id="question' + id + '" class="panel panel-default"></div>');
    $('#question' + id).append('<div class="panel-heading row" role="tab" id="heading' + id + '"></div>');

    $('#heading' + id).append('<div class="col-xs-10" class="padding:0"><h4><input id="chk' + id + '" type="checkbox" value="' + id + '" title="Include in questionnaire and quiz"> <a  id="qText' + id + '" class="collapsed" role="button" data-toggle="collapse" href="#collapse' + id + '" aria-expanded="false" aria-controls="collapse' + id + '">' + data.text + '</a></h4></div>');
    $('#heading' + id).append('<div class="col-xs-2"><span class="pull-right"><button data-qid="'+id+'" data-toggle="modal" data-target="#deleteQuestionModal" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></div>');

    $('#question' + id).append('<div id="collapse' + id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + id + '"></div>');
    $('#collapse' + id).append('<div id="choices' + id + '" class="panel-body"></div>');

    $('#choices' + id).append('<button type="button" class="btn btn-success btn-choice pull-right" data-toggle="modal" data-target="#dialogModal" data-qid="' + id + '">Add choice</button>');

    // If question is visible, check visibility checkbox
    $('#chk' + id).prop('checked', data.visible);

    // If there are any choice, add them under question
    if (Array.isArray(data.choices) && data.choices.length) {
        $.each(data.choices, function (index, choice) {
            showChoice(choice);
        });
    }
}

// Add choice under question in question list
function showChoice(choice) {
    var id = choice.questionId;
    $('#choices' + id).append('<button type="button" class="btn btn-default btn-choice">' + choice.text + '</button> ');
}

// Remove all questions and their choices from list
function unloadQuiz() {
    $('#accordion').empty();
    $('#addQuestionBtn').remove();
}

// Change  question visiblity in Db
function setQuestionVisibility(questionId, visible) {
    $.ajax({
        type: 'POST',
        url: apiBaseAddress + 'Questions/'+ questionId + '?visible='+visible,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        }
    });
}

// Delete question from db and remove from question list
function deleteQuestion(id) {
    $.ajax({
        type: "DELETE",
        url: apiBaseAddress + 'Questions/' + id,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function () {
            $('#question' + id).remove();
            showMessage('success', 'Question deleted!');
        },
        error: function () {
            showMessage('danger', 'Error! Question was not deleted!');
        }
    });
}