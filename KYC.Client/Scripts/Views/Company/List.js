﻿/* See system function requirements - function S2.COMPANY.LIST - for more information
 * File includes Datatables v.1.10.12 plugin
 * Used in Company/Index view */

$.fn.dataTable.ext.errMode = 'none';    // Disables datatables eror alert window

$('#companyTable').on('error.dt', function (e, settings, techNote, message) {
    console.log('An error has been reported: ', message);
}).DataTable({      // initialize data tables plugin
    responsive: true,
    ajax: {
        url: apiBaseAddress + 'Companies',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        dataSrc: ''
    },
    columns: [
        {
            data: 'name',
            render: function (data, type, company) {
                return '<a href="' + clientBaseAddress + 'Company/Edit/' +  + encodeURIComponent(company.companyId) + '">' + data + '</a>';
            }
        },
        {
            data: 'address',
            render: $.fn.dataTable.render.text()
        },
        {
            data: 'email',
            render: $.fn.dataTable.render.text()
        },
        {
            data: 'phone',
            render: $.fn.dataTable.render.text()
        }
    ],
    'bInfo': false,         // disable "showing" text (from which to which number element is included in table)
    'pageLength': 20,       // row number in one page
    'lengthChange': false,  // disable changing the row count in page
    'language':{            // override message text
        'emptyTable': 'You don’t have any companies created.',
        "zeroRecords": "No companies were found!"
    }
});