﻿/* See system function requirements - function S2.COMPANY.EDIT - for more information
 * Used in Company/Edit view */


$(document).ready(function () {
    if (statusCode === '201') {
        showMessage('info', 'Company created!');
    }

    GetCompany(companyId);


    // When double-click on user name in employee list
    $('#usersLbx').dblclick(function () {
        $("select option:selected").each(function () {
            $(this).appendTo('#adminsLbx');
        });
    });

    // When double-click on user name in administrator list
    $('#adminsLbx').dblclick(function () {
        $("select option:selected").each(function () {
            $(this).appendTo('#usersLbx');
        });
    });

    // When Save changes button is pressed
    $('#addBtn').click(function () {
        saveCompany(companyId);
    });

    // When Discard changes is pressed
    $('#discardBtn').click(function () {
        GetCompany(companyId);
    });
});

// Load company info from API
function GetCompany(id) {
    var originalEmailHost;
    $.ajax({
        type: "GET",
        url: apiBaseAddress + 'Companies/' + id,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            $('#nameTxt').val(data.name);
            $('#addressTxt').val(data.address);
            $('#phoneTxt').val(data.phone);
            $('#usersLbx').empty();
            $('#adminsLbx').empty();

            // Company e-mail host is not editable
            var email = data.email.split('@');
            $('#emailTxt').val(email[0]);
            $('#emailTxt').attr('maxlength', 255 - email[1].length - 1);
            $('#emailHostLbl').text(email[1]);

            // Add users to employee list
            $.each(data.users, function (index, user) {
                $('#usersLbx').append($('<option>').text(user.name).val(user.userId));
            });
            // Add existing company admins
            $.each(data.admins, function (index, adminId) {
                $("#usersLbx option[value='" + adminId + "']").appendTo('#adminsLbx');
            });
            $('#usersLbx').attr('size', '4');
            $('#adminsLbx').attr('size', '4');
        },
        error: function (error) {
            // If user is not company or system administrator
            if (error.status === 403) {
                window.location.replace(clientBaseAddress + 'Colleagues?status_code=401');
            }
            else {
                window.location.replace(clientBaseAddress + 'Colleagues');
            }
        }
    });
}

// Updates company info
function saveCompany(id) {
    var adminIds = [];
    $('#adminsLbx option').each(function () {
        adminIds.push($(this).val());
    });
    $.ajax({
        type: "PUT",
        url: apiBaseAddress + 'Companies/' + id,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({
            companyId: id,
            name: $('#nameTxt').val(),
            address: $('#addressTxt').val(),
            email: $('#emailTxt').val() + '@' + $('#emailHostLbl').text(),
            phone: $('#phoneTxt').val(),
            admins: adminIds
        }),
        success: function (data) {
            showMessage('success', 'Company data saved successfully!');
        },
        error: function (error) {
            showMessage('danger', 'Company data were not saved!' + getModelErrors(error));
        }
    });
}