﻿/* See system function requirements - function S3.QUIZ.COLLEAGUE - for more information
 * Used in Quiz/Company view */

$(document).ready(function () {
    LoadQuestion();
    LoadStatusBar('#progress', 'User');
});

// When click on any of choices
$('#choices').on('click', 'button', function (evt) {
    $('#choices button').attr('disabled', true);
    $(this).removeClass('btn-default').addClass('btn-warning');
    SaveAnswer($(this).parent().data('userId'), $(this).data('cid'), $(this));
});

// When click "Skip" button
$('#skipBtn').click(function () {
    LoadQuestion();
});

// Load random question from API
function LoadQuestion()
{
    $.ajax({
        type: "GET",
        url: apiBaseAddress + 'Quiz',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            ShowQuestion(data);
            $('.thumbnail').removeClass('hidden');
        },
        error: function (error) {
            // Message saying, that colleagues have not filled questionnaire, is shown
            $('#noQuestions').show('fade');
            $('#skipBtn').hide();
        }
    });
}

// Display question and choices, loaded from API
function ShowQuestion(data) {
    var $container = $('#questionDiv');
    var $choicesDiv = $('#choices');
    $choicesDiv.empty();
    $container.hide('fade', 'fast', function () {
        $('#profileImg').attr("src", data.userImageUrl + '?sz=250');
        $('.thumbnail').show();
        $container.find('#question').text(data.questionText);
        $('#nameLbl').text(data.userName);

        $.each(Shuffle(data.choices), function (index, choice) {
            $choicesDiv.append('<button type="button" class="btn btn-default btn-choice" data-cid="' + choice.choiceId + '">' + choice.text + '</button>');
        });
        $choicesDiv.data('userId', data.userId);
        $container.show('fade', 'fast');
    });
}

// Save quiz answer
function SaveAnswer(colleagueId, choiceId, button) {
    $.ajax({
        type: "POST",
        url: apiBaseAddress + 'Quiz',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({
            colleagueProfileId: colleagueId,
            choiceId: choiceId
        }),
        success: function (answer) {
            button.removeClass();
            // if answer is correct
            if (answer == choiceId) {
                // Update status bar correct answer count
                CorrectAnswers++;
                // Color choice button green
                button.addClass('btn btn-success btn-choice');
                // Do vertical bouncing animation
                button.parent().parent().effect('bounce','swing',1500);
            }
            else {
                // Color choice button red
                button.addClass('btn btn-danger btn-choice');
                // Color correct answer green
                var correctButton = $('#choices').find('[data-cid="' + answer + '"]');
                correctButton.removeClass();
                correctButton.addClass('btn btn-success btn-choice');
                // Do horizontal shaking animation
                button.parent().parent().effect('shake', 'swing', 1000);
            }
            // After 1.5 sec change question
            setTimeout(function () {
                // Update status bar total answer count
                TotalAnswers++;
                // Visualize updated anwer count
                updateStatusBar('#progress');
                // Load another random question
                LoadQuestion();
            }, 1500);
        },
        error: function (error) {
            // Redirect away from quiz
            window.location.href(clientBaseAddress + 'Colleagues');
        }
    });
}

// Fisher-Yates Shuffle ( adapted from https://bost.ocks.org/mike/shuffle/ )
// Generates random permutation of array elements
function Shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}