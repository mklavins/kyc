﻿/* See system function requirements - function S3.QUIZ.SELF - for more information
 * Used in Quiz/Company view */

var questions;          // All company questions
var questionIndex = 0;  // Currently showing question Id

$(document).ready(function () {
    GetQuestionnaire();
});

// Wnen chosing question
$(document).on('change', 'input:radio[id^="option"]', function (event) {
    questionIndex = parseInt($(this).val());
    ShowQuestion(questionIndex);
});

// When click on any of choices
$('#choices').on('click', 'button', function (evt) {
    $('#choices button').attr('disabled', true);
    $('#choices button').removeClass();
    $('#choices button').addClass('btn btn-default btn-choice');
    $(this).removeClass('btn-default');
    $(this).addClass('btn btn-success btn-choice');

    SaveAnswer($(this).parent().data('qid'), $(this).data('cid'));
});

$('#nextBtn').click(function () {
    moveToNext(++questionIndex);
});

$('#prevBtn').click(function () {
    moveToNext(--questionIndex);
});

// Get all visible company questions with choices and their choices
function GetQuestionnaire()
{
    $.ajax({
        type: "GET",
        url: apiBaseAddress + 'Questions/?answers=true&includeEmpty=false',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        success: function (data) {
            if (data != null) {
                questions = data;
                CreateQuestionBar(questions);
                ShowQuestion(0);
            }
            else {
                $('#noQuestions').show('fade');
            }
        },
        error: function (error) {
            showMessage('danger', 'Error while loading questionnaire!');
        }
    });
}

// Create question-switching bar
function CreateQuestionBar(data) {
    var btnWidth = (100 / data.length).toFixed(2);
    // Create new button for every question
    $.each(data, function (index, question) {
        $('#statusBar').append('<label class="btn btn-warning" style="width:' + btnWidth + '%" id="q' + question.questionId + '">'+
                               '<input type="radio" name="options" id="option' + question.questionId + '" value="' + index + '"></label>');

        // If user has answered this question
        if (question.answerId != 0) {
            $('#q' + question.questionId).removeClass();
            $('#q' + question.questionId).addClass('btn btn-success');
        }
    });
}

// Display cpompany question
function ShowQuestion(id) {
    var question = questions[id];
    var $container = $('#questionDiv');
    var $choicesDiv = $('#choices');
    $choicesDiv.empty();
    $container.hide('fade', 'fast', function () {
        $container.find('#question').text(id+1+') '+question.text);

        $.each(question.choices, function (index, choice) {
            $choicesDiv.append('<button type="button" class="btn btn-choice" data-cid="' + choice.choiceId + '">' + choice.text + '</button>');

            if (choice.choiceId == question.answerId)
                $choicesDiv.find('button').last().addClass('btn-success');
            else
                $('#choices button').last().addClass('btn-default');
        });
        $choicesDiv.data('qid', question.questionId);
        $('#q' + question.questionId).button('toggle');
        $container.show('fade', 'fast');
    });
}

// Save answer made by user
function SaveAnswer(questionId, choiceId) {
    $.ajax({
        type: "POST",
        url: apiBaseAddress + 'Questions/' + questionId,
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
        },
        data: JSON.stringify({ choiceId: choiceId }),
        success: function (data) {
            questions[questionIndex].answerId = choiceId;
            $('#q' + questionId).removeClass('btn-warning');
            $('#q'+questionId).addClass('btn-success');
            moveToNext(++questionIndex);
        },
        error: function (error) {
        }
    });
}

// Show next question in line
function moveToNext(id) {
    var maxIndex = questions.length - 1;
    if (id > maxIndex)
        id = 0;
    else if (id < 0)
        id = maxIndex;

    ShowQuestion(id);
    questionIndex = id;
}