﻿using KYC.Client.Helpers;
using KYC.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KYC.Client.Controllers
{
    /// <summary>
    /// Controller for company management- adding, editing
    /// </summary>
    [RoutePrefix("Company")]
    public class CompanyController : BaseController
    {
        // Company list
        // S2.COMPANY.LIST
        [Administrator]
        public ActionResult Index()
        {
            return View();
        }

        // Adding new company
        // S2.COMPANY.ADD
        [Route("Add")]
        [Administrator]
        public ActionResult Add()
        {
            return View();
        }

        // Edit company
        // S2.COMPANY.EDIT
        [Route("Edit/{companyId:int}")]
        public ActionResult Edit(int companyId=0)
        {
            ViewBag.StatusCode = Request.QueryString["status_code"] ?? "";
            ViewBag.CompanyId = companyId;
            return View();
        }
    }

    // Authentication filter attribute- mark controller methods, that are accessable only for system admin
    public class Administrator : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var  container = new UserInfoContainer();

            if (!(bool)container.IsAdmin)
            {
                filterContext.Result = new RedirectResult(BaseAddress.Client + "Colleagues");
            }
        }
    }
}