﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KYC.Client.Controllers
{
    /// <summary>
    /// Controller for questionaire and quiz
    /// </summary>
    public class QuizController : BaseController
    {
        // Quiz - user answers questions about his colleagues
        // S3.QUIZ.COLLEAGUES
        public ActionResult Index()
        {
            return View();
        }

        // Questionnaire - user answers questions about himself
        // S3.QUIZ.SELF
        public ActionResult Company()
        {
            return View();
        }
    }
}