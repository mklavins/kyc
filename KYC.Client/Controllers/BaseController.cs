﻿using KYC.Client.Helpers;
using System;
using System.Web.Mvc;

namespace KYC.Client.Controllers
{
    // Base controller for maintaining user authentication state and permissions
    public abstract class BaseController : Controller
    {
        protected IUserInfoContainer UserInfo = new UserInfoContainer();

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            // User has to be logged in to access any of inherited controllers
            if (UserInfo.IsAdmin == null)
                filterContext.Result = new RedirectResult(Url.Action("Index", "SignIn", new { status_code = 401 }));

            // Wherher user is system administrator
            ViewBag.isAdmin = UserInfo.IsAdmin;
        }
    }
}