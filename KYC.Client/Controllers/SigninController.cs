﻿using KYC.Client.Helpers;
using KYC.Client.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KYC.Client.Controllers
{
    /// <summary>
    /// Controller for user authentication functions.
    /// </summary>
    public class SignInController : Controller
    {
        private IUserInfoContainer userContainer;
        public SignInController()
        {
            userContainer = new UserInfoContainer();
        }

        // Returns login form
        // S1.USER.AUTHENTICATE
        public ActionResult Index()
        {
            // Code for alert message
            string statusCode = Request.QueryString["status_code"] ?? "";

            if (statusCode != "202")
                userContainer.Clear();

            ViewBag.StatusCode = statusCode;
            return View();
        }

        [HttpGet]
        // Redirects user to Google authentication
        // S1.AUTHENTICATE
        public ActionResult SignIn(string redirect)
        {
            // Setting up address for Google to redirect
            var redirectTo = Uri.EscapeUriString(BaseAddress.Client + redirect);
            var uri = new Uri(BaseAddress.API + "Account/ExternalLogin?provider=Google" +
                "&response_type=token&client_id=self&redirect_uri=" + redirectTo + "&state=-tA5EkceFw4AdKM3yyJqBBOQQCI5K3yK5R0Lfm9XHGc1")
                .ToString();

            return Redirect(uri);
        }

        [HttpPost]
        // Creates session for authenticated user to manage permissions
        // S1.USER.AUTHENTICATE
        public ActionResult SignedIn(UserInfoViewModel userInfo)
        {
            if (userInfo == null)
                return  new HttpStatusCodeResult(HttpStatusCode.BadRequest, "");
            
            userContainer.Clear();
            // Whether user is system administrator
            userContainer.IsAdmin = userInfo.IsAdmin;

            return new HttpStatusCodeResult(HttpStatusCode.OK, "");
        }

        [HttpGet]
        [Route("SignOut")]
        // Removes user session and redirects user to login page
        // S1.USER.SIGNOUT
        public ActionResult SignOut()
        {
            string statusCode = Request.QueryString["status_code"] ?? "";
            userContainer.Clear();
            return RedirectToAction("Index", new { status_code = statusCode });
        }
    }
}