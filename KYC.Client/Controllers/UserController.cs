﻿using System.Web.Mvc;

namespace KYC.Client.Controllers
{
    /// <summary>
    /// Controller for user management - profile viewing, editing, colleague list
    /// </summary>
    public class UserController : BaseController
    {
        // Colleague list
        // S1.USER.LIST
        [HttpGet]
        [Route("Colleagues")]
        public ActionResult UsersList()
        {
            ViewBag.StatusCode = Request.QueryString["status_code"] ?? "";
            return View();
        }

        // View colleague profile
        // S1.USER.PROFILE.VIEW
        [HttpGet]
        [Route("User/Profile/View/{userId?}", Name = "UsersList")]
        public ActionResult ViewUserProfile(string userId)
        {
            ViewBag.User = userId ?? " ";
            return View();
        }

        // Edit user profile information, view profile status info
        // S1.USER.PROFILE.EDIT
        [HttpGet]
        [Route("User/Profile/Edit")]
        public ActionResult EditUserProfile()
        {
            return View();
        }
    }
}