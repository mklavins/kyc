﻿using System.Web;

namespace KYC.Client.Helpers
{
    // Container for managing user session on server
    public interface IUserInfoContainer
    {
        // Is user system administrator
        object IsAdmin { get; set; }
        // Clear all user session collection data
        void Clear();
    }
    
    public class UserInfoContainer : IUserInfoContainer
    {
        private System.Web.SessionState.HttpSessionState session = HttpContext.Current.Session;

        public object IsAdmin
        {
            get { return (session != null  && session["isAdmin"] != null) ? session["isAdmin"] : null; }
            set { if (session != null) session["isAdmin"] = value; }
        }

        public void Clear()
        {
            session.Clear();
        }
    }
}