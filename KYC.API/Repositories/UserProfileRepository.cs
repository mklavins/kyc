﻿using System;
using System.Linq;
using KYC.API.Models;
using KYC.API.Entities;
using KYC.API.Interfaces;
using KYC.API.Dtos;
using System.Collections.Generic;
using AutoMapper;

namespace KYC.API.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private ApplicationDbContext context;

        public UserProfileRepository(ApplicationDbContext _context)
        {
            context = _context;
        }

        /// <summary>
        /// Gets colleagues from selected company. Only accepted user profiles are included in list.
        /// Optionally one user can be excluded (eg. user who makes request).
        /// </summary>
        public IQueryable<UserProfile> GetUsers(int companyId)
        {
            return context.UserProfiles.Where(u => u.CompanyId == companyId);
        }

        /// <summary>
        /// Returns user profile info about user
        /// </summary>
        public UserProfile GetUser(string userId)
        {
            return context.UserProfiles
                   .Where(u => u.UserId == userId)
                   .FirstOrDefault();
        }

        /// <summary>
        /// Returns user profile info about user
        /// </summary>
        public UserProfile GetUser(int userProfileId)
        {
            return context.UserProfiles
                   .Where(u => u.Id == userProfileId)
                   .FirstOrDefault();
        }

        /// <summary>
        /// Gets user Name
        /// </summary>
        public string GetUserName(string userId)
        {
            return context.UserProfiles
                          .Where(u => u.UserId == userId)
                          .Select(u => u.Name)
                          .FirstOrDefault();
        }

        /// <summary>
        /// Returns user email from userId
        /// </summary>
        public string GetUserEmail(string userId)
        {
            return context.Users
                          .Where(u => u.Id == userId)
                          .Select(uu => uu.Email)
                          .FirstOrDefault();
        }

        /// <summary>
        /// Returns company id for user
        /// </summary>
        public int GetUserCompanyId(string userId)
        {
            return context.UserProfiles
                          .Where(u => u.UserId == userId)
                          .Select(u => u.CompanyId)
                          .FirstOrDefault();
        }

        /// <summary>
        /// Gets answers user provided for company quiz about himself
        /// </summary>
        public IQueryable<Answer> GetAnswers(int userProfileId)
        {
            return context.Answers.Where(a => a.UserProfileId == userProfileId && a.Choice.Question.Visible == true);
        }

        /// <summary>
        /// Returns true, if User (userId) is Company(companyId) admin
        /// </summary>
        public bool IsCompanyAdmin(string userId, int companyId = 0)
        {
            var user = GetUser(userId);

            if (companyId == 0)
                return user.IsCompanyAdmin;

            return (companyId == user.CompanyId && user.IsCompanyAdmin);
        }

        /// <summary>
        /// Adds user profile to data base whan user is registering in system.
        /// </summary>
        public void Add(UserProfile userProfile)
        {
            context.UserProfiles.Add(userProfile);
        }

        /// <summary>
        /// Saves user profile changes in database
        /// </summary>
        public void Update(string userId, UserProfile userProfile)
        {
            var userProfileFromDb = GetUser(userId);

            if (userProfileFromDb != null)
            {
                // Make all possible changes in user profile 
                Mapper.Map(userProfile, userProfileFromDb);
            }
        }

        /// <summary>
        /// Set user IsCompanyAdmin value to 'isAdmin'
        /// </summary>
        public void SetAdmin(string userId, bool isAdmin)
        {
            GetUser(userId).IsCompanyAdmin = isAdmin;
        }

        /// <summary>
        /// Returns user profile status info
        /// </summary>
        public UserProfileStatusDto GetProfileStatus(string userId)
        {
            var user = GetUser(userId);

            return new UserProfileStatusDto
            {
                ProfileConfirmed = user.ProfileConfirmed,
                QuestionsAnswered = context.Answers.Where(u => u.UserProfileId == user.Id && u.Choice.Question.Visible).Any()
            };
        }

        /// <summary>
        /// Update user profile name with new value "name"
        /// </summary>
        public void UpdateName(UserProfile userProfile, string name)
        {
            userProfile.Name = name;
        }

        /// <summary>
        /// Update user profile image Url with new value "imageUrl"
        /// </summary>
        public void UpdateImageUrl(UserProfile userProfile, string imageUrl)
        {
            userProfile.ImageUrl = imageUrl;
        }

        /// <summary>
        /// Update user profile Google+ profile Url with new value "profileUrl"
        /// </summary>
        public void UpdateGooglePlusProfileurl(UserProfile userProfile, string profileUrl)
        {
            userProfile.GooglePlusProfileUrl = profileUrl;
        }
    }
}