﻿using KYC.API.Interfaces;
using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace KYC.API.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        private IUserProfileRepository userProfileRepository;
        private ICompanyRepository companyRepository;
        private IQuizRepository quizRepository;

        public UnitOfWork(ApplicationDbContext _context)
        {
            context = _context;
        }

        public IUserProfileRepository UserProfile
        {
            get
            {
                return userProfileRepository = userProfileRepository ?? new UserProfileRepository(context);
            }
        }

        public ICompanyRepository Company
        {
            get
            {
                return companyRepository = companyRepository ?? new CompanyRepository(context);
            }
        }

        public IQuizRepository Quiz
        {
            get
            {
                return quizRepository = quizRepository ?? new QuizRepository(context);
            }
        }

        /// <summary>
        /// Save all changes made in all unitOfWork repositories
        /// </summary>
        public void Save()
        {
            context.SaveChanges();
        }
    }
}