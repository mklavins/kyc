﻿using AutoMapper;
using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Interfaces;
using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Data.Entity;
using System.Web.Security.AntiXss;

namespace KYC.API.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private ApplicationDbContext context;

        public CompanyRepository(ApplicationDbContext _context)
        {
            this.context = _context;
        }

        #region Company
        /// <summary>
        /// Get all companies in system
        /// </summary>
        public IQueryable<Company> GetCompanies()
        {
            return context.Companies;
        }

        /// <summary>
        /// Get company data and, if necessary- all registered company employees
        /// </summary>
        public Company GetCompany(int companyId, bool includeUsers = false)
        {
            if (includeUsers)
                return context.Companies
                    .Include(x => x.Users)
                    .Where(c => c.CompanyId == companyId)
                    .FirstOrDefault();

            return context.Companies
                .Where(c => c.CompanyId == companyId)
                .FirstOrDefault();
        }

        /// <summary>
        /// Get company by its e-mail address host
        /// </summary>
        public Company GetCompany(string email)
        {
            // Extract host from e-mail address
            if (email.IndexOf('@') != -1)
            {
                var emailHost = email.Split('@')[1];
                return (context.Companies.Where(c => c.Email.EndsWith("@" + emailHost)).FirstOrDefault());
            }
            return null;
        }

        /// <summary>
        /// Whether there is a company with such Id
        /// </summary>
        public bool CompanyExists(int companyId)
        {
            return context.Companies.Any(c => c.CompanyId == companyId);
        }

        /// <summary>
        /// Whether there is a company with such name
        /// </summary>
        public bool CompanyExists(string name)
        {
            return context.Companies.Any(c => c.Name == name);
        }

        /// <summary>
        /// Get company id from its e-mail host
        /// Return 0, if company doesn't exist
        /// </summary>
        public int CompanyForEmailExists(string email)
        {
            if (email.IndexOf('@') != -1)
            {
                var emailHost = email.Split('@')[1];
                return (context.Companies.Where(c => c.Email.EndsWith("@" + emailHost)).Select(cc => cc.CompanyId).FirstOrDefault());
            }
            return 0;
        }

        /// <summary>
        /// Adds company to db
        /// </summary>
        public void AddCompany(Company company)
        {
            context.Companies.Add(company);
        }
        #endregion

        #region Questions and answers
        /// <summary>
        /// Gets all questions that belong to company companyId with or without choces.
        /// </summary>
        public IQueryable<Question> GetQuestions(int companyId, bool includeChoices = false)
        {
            if (includeChoices)
                return context.Questions.Include(q => q.Choices).Where(qq => qq.CompanyId == companyId);

            return context.Questions.Where(qq => qq.CompanyId == companyId);
        }

        /// <summary>
        /// Gets question with specific questionId with or without choces.
        /// </summary>
        public Question GetQuestion(int questionId, bool includeChoices = false)
        {
            if (includeChoices)
                return context.Questions.Include(q => q.Choices).Where(qq => qq.QuestionId == questionId).FirstOrDefault();

            return context.Questions.Where(q => q.QuestionId == questionId).FirstOrDefault();
        }

        /// <summary>
        /// Get choice by its choice ID
        /// </summary>
        public Choice GetChoice(int choiceId)
        {
            return context.Choices.Where(c => c.ChoiceId == choiceId).FirstOrDefault();
        }

        /// <summary>
        /// Create new question and add it to company.
        /// </summary>
        public void AddQuestion(Company company, Question question)
        {
            company.Questions.Add(question);
        }

        /// <summary>
        /// If visibility is set to true, question is not shown in questionnaire and quiz for users.
        /// All question answers still are included in statistics.
        /// </summary>
        public void SetQuestionVisibility(int questionId, bool visible)
        {
            var question = context.Questions.Where(q => q.QuestionId == questionId).FirstOrDefault();
            if (question != null && question.Visible != visible)
                question.Visible = visible;
        }

        /// <summary>
        /// Delete question from DB.
        /// </summary>
        public void DeleteQuestion(Question question)
        {
            // Find all quiz answers about question
            var quizAnswers = context.QuizAnswers
                                     .Join(context.Choices,
                                        qa => qa.ChoiceId,
                                        c => c.ChoiceId,
                                        (qa, c) =>
                                            new { c, qa })
                                            .Where(x => x.c.QuestionId == question.QuestionId)
                                     .Select(x => x.qa);

            // Delete all question quiz answers
            context.QuizAnswers.RemoveRange(quizAnswers);
            // Delete question
            context.Questions.Remove(question);
        }

        /// <summary>
        /// Create new choice and add it to question.
        /// </summary>
        public void AddChoice(Question question, Choice choice)
        {
            question.Choices.Add(choice);
        }
        
        /// <summary>
        /// Add user answer to question.
        /// </summary>
        public void SaveAnswer(Choice choice, int userProfileId)
        {
            // Find previous answer made by user about himself
            var oldChoiceId = context.Answers
                            .Where(a => a.UserProfileId == userProfileId &&
                                        a.Choice.QuestionId == choice.QuestionId)
                            .OrderByDescending(x => x.DateCreated)
                            .Select(x => x.ChoiceId)
                            .FirstOrDefault();

            // If user has changed his answer
            if (oldChoiceId != choice.ChoiceId)
            {
                // Add new answer to db
                var answer = new Answer()
                {
                    Choice = choice,
                    UserProfileId = userProfileId,
                    DateCreated = DateTime.UtcNow
                };
                context.Answers.Add(answer);
            }
        }
        #endregion
    }
}