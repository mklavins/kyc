﻿using KYC.API.Entities;
using KYC.API.Interfaces;
using KYC.API.Models;
using System;
using System.Linq;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KYC.API.Repositories
{
    public class QuizRepository : IQuizRepository
    {
        // Db context instance, used to access db data
        private ApplicationDbContext context;

        public QuizRepository(ApplicationDbContext _context)
        {
            context = _context;
        }

        /// <summary>
        /// Returns randomly selected colleague answer with corresponding
        /// question and question choices
        /// </summary>
        public Answer GetRandomAnswer(int companyId, int excludedUserProfileId)
        {
            // Randomly select colleague id for quiz
            // Only colleagues, who have answered any quiz question, are included
            // User himself is excluded
            var randomColleague = context.UserProfiles
                          .Where(u => u.CompanyId == companyId 
                                && u.Id != excludedUserProfileId
                                && context.Answers.Where(a => a.UserProfileId == u.Id).Any())
                          .OrderBy(r => Guid.NewGuid())
                          .Select(x => x.Id)
                          .FirstOrDefault();

            // If user has at least one colleague
            if (randomColleague != 0)
                // Get random answer with visible question, that randomColleague
                // has answered
                return context.Answers
                              .Include(x => x.Choice.Question.Choices)
                              .Where(a => a.UserProfileId == randomColleague &&
                                          a.Choice.Question.Visible)
                              .OrderBy(r => Guid.NewGuid())
                              .FirstOrDefault();
            else
                return null;
        }

        /// <summary>
        /// Gets choice from its unique id from db
        /// Choice question is also included
        /// </summary>
        public Choice GetChoice(int choiceId)
        {
            return context.Choices
                .Include(a => a.Question.Choices)
                .Where(c => c.ChoiceId == choiceId)
                .FirstOrDefault();
        }

        /// <summary>
        /// Return correct choice id for a question, that user has answered
        /// </summary>
        public int GetAnswer(int colleagueProfileId, int questionId)
        {
            return context.Answers
                          .Where(a => a.UserProfileId == colleagueProfileId &&
                                      a.Choice.QuestionId == questionId)
                          .OrderByDescending(aa => aa.DateCreated) // to find current answer
                          .Select(aaa => aaa.ChoiceId)
                          .FirstOrDefault();
        }

        /// <summary>
        /// Return total quiz answer count that user has left
        /// </summary>
        public int GetTotalUserAnswerCount(int userProfileId)
        {
            return context.QuizAnswers.Where(qa => qa.UserProfileId == userProfileId).Count();
        }

        /// <summary>
        /// Return total quiz answer count that colleagues have left about user
        /// </summary>
        public int GetTotalColleagueAnswerCount(int userProfileId, int companyId)
        {
            return context.QuizAnswers.Where(qa => qa.ColleagueProfileId == userProfileId 
                                                && qa.User.CompanyId == companyId)
                                                .Count();
        }

        /// <summary>
        /// Return correct quiz answer count that user has left
        /// </summary>
        public int GetCorrectUserAnswerCount(int userProfileId)
        {
            return context.QuizAnswers.Where(qa => qa.UserProfileId == userProfileId 
                                                && qa.Correct).Count();
        }

        /// <summary>
        /// Return correct quiz answer count that colleagues have left about user
        /// </summary>
        public int GetCorrectColleagueAnswerCount(int userProfileId, int companyId)
        {
            return context.QuizAnswers.Where(qa => qa.ColleagueProfileId == userProfileId 
                                                        && qa.User.CompanyId == companyId
                                                        && qa.Correct).Count();
        }

        /// <summary>
        /// Save quiz answer to db
        /// </summary>
        public void AnswerQuizQuestion(QuizAnswer quizAnswer)
        {
            quizAnswer.DateCreated = DateTime.UtcNow;
            context.QuizAnswers.Add(quizAnswer);
        }
    }
}