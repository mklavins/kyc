﻿using KYC.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KYC.API.Dtos;

namespace KYC.API.Interfaces
{
    public interface IQuizRepository
    {
        /// <summary>
        /// Returns randomly selected colleague answer with corresponding
        /// question and question choices
        /// </summary>
        Answer GetRandomAnswer(int companyId, int userProfileId);

        /// <summary>
        /// Gets choice from its unique id from db
        /// Choice question is also included
        /// </summary>
        Choice GetChoice(int choiceId);

        /// <summary>
        /// Save quiz answer to db
        /// </summary>
        void AnswerQuizQuestion(QuizAnswer quizAnswer);

        /// <summary>
        /// Return correct choice id for a question, that user has answered
        /// </summary>
        int GetAnswer(int userProfileId, int questionId);

        /// <summary>
        /// Return total quiz answer count that user has left
        /// </summary>
        int GetTotalUserAnswerCount(int userProfileId);

        /// <summary>
        /// Return total quiz answer count that colleagues have left about user
        /// </summary>
        int GetTotalColleagueAnswerCount(int userProfileId, int companyId);

        /// <summary>
        /// Return correct quiz answer count that user has left
        /// </summary>
        int GetCorrectUserAnswerCount(int userProfileId);

        /// <summary>
        /// Return correct quiz answer count that colleagues have left about user
        /// </summary>
        int GetCorrectColleagueAnswerCount(int userProfileId, int companyId);
    }
}
