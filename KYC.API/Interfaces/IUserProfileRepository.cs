﻿using KYC.API.Dtos;
using KYC.API.Entities;
using System.Collections.Generic;
using System.Linq;

namespace KYC.API.Interfaces
{
    public interface IUserProfileRepository
    {
        /// <summary>
        /// Gets colleagues from selected company. Only accepted user profiles are included in list.
        /// Optionally one user can be excluded (eg. user who makes request).
        /// </summary>
        IQueryable<UserProfile> GetUsers(int companyId);

        /// <summary>
        /// Returns user profile info about user
        /// </summary>
        UserProfile GetUser(string userId);

        /// <summary>
        /// Returns user profile info about user
        /// </summary>
        UserProfile GetUser(int userProfileId);

        /// <summary>
        /// Gets user Name
        /// </summary>
        string GetUserName(string userId);

        /// <summary>
        /// Returns user email from userId
        /// </summary>
        string GetUserEmail(string userId);

        /// <summary>
        /// Returns company id for user
        /// </summary>
        int GetUserCompanyId(string userId);

        /// <summary>
        /// Gets answers user provided for company quiz about himself
        /// </summary>
        IQueryable<Answer> GetAnswers(int userProfileId);

        /// <summary>
        /// Returns true, if User (userId) is Company(companyId) admin
        /// </summary>
        bool IsCompanyAdmin(string userId, int companyId = 0);

        /// <summary>
        /// Adds user profile to data base whan user is registering in system.
        /// </summary>
        void Add(UserProfile userProfile);

        /// <summary>
        /// Saves user profile changes in database
        /// </summary>
        void Update(string userId, UserProfile userProfile);

        /// <summary>
        /// Set user IsCompanyAdmin value to 'isAdmin'
        /// </summary>
        void SetAdmin(string userId, bool isAdmin);

        /// <summary>
        /// Returns user profile status info
        /// </summary>
        UserProfileStatusDto GetProfileStatus(string userId);

        /// <summary>
        /// Update user profile name with new value "name"
        /// </summary>
        void UpdateName(UserProfile userProfile, string name);

        /// <summary>
        /// Update user profile image Url with new value "imageUrl"
        /// </summary>
        void UpdateImageUrl(UserProfile userProfile, string imageUrl);

        /// <summary>
        /// Update user profile Google+ profile Url with new value "profileUrl"
        /// </summary>
        void UpdateGooglePlusProfileurl(UserProfile userProfile, string profileUrl);
    }
}