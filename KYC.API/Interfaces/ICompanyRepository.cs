﻿using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Models;
using System.Collections.Generic;
using System.Linq;

namespace KYC.API.Interfaces
{
    public interface ICompanyRepository
    {
        /// <summary>
        /// Get all companies in system
        /// </summary>
        IQueryable<Company> GetCompanies();

        /// <summary>
        /// Get company data and, if necessary- all registered company employees
        /// </summary>
        Company GetCompany(int companyId, bool includeUsers = false);

        /// <summary>
        /// Get company by its e-mail address host
        /// </summary>
        Company GetCompany(string email);

        /// <summary>
        /// Whether there is a company with such Id
        /// </summary>
        bool CompanyExists(int companyId);

        /// <summary>
        /// Whether there is a company with such name
        /// </summary>
        bool CompanyExists(string name);

        /// <summary>
        /// Get company id from its e-mail host
        /// Return 0, if company doesn't exist
        /// </summary>
        int CompanyForEmailExists(string email);

        /// <summary>
        /// Adds company to db
        /// </summary>
        void AddCompany(Company companyDto);

        /// <summary>
        /// Gets all questions that belong to company companyId with or without choces.
        /// </summary>
        IQueryable<Question> GetQuestions(int companyId, bool includeChoices = false);

        /// <summary>
        /// Gets question with specific questionId with or without choces.
        /// </summary>
        Question GetQuestion(int questionId, bool includeChoices = false);

        /// <summary>
        /// Create new question and add it to company.
        /// </summary>
        void AddQuestion(Company company, Question question);

        /// <summary>
        /// If visibility is set to true, question is not shown in questionnaire and quiz for users.
        /// All question answers still are included in statistics.
        /// </summary>
        void SetQuestionVisibility(int questionId, bool visible);

        /// <summary>
        /// Delete question from DB.
        /// </summary>
        void DeleteQuestion(Question question);

        /// <summary>
        /// Get choice by its choice ID
        /// </summary>
        Choice GetChoice(int choiceId);

        /// <summary>
        /// Create new choice and add it to question.
        /// </summary>
        void AddChoice(Question question, Choice choice);

        /// <summary>
        /// Add user answer to question.
        /// </summary>
        void SaveAnswer(Choice choice, int userProfileId);
    }
}
