﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KYC.API.Interfaces
{
    public interface IUnitOfWork
    {
        IUserProfileRepository UserProfile { get; }
        ICompanyRepository Company { get; }
        IQuizRepository Quiz { get; }

        /// <summary>
        /// Save all changes made in all unitOfWork repositories
        /// </summary>
        void Save();
    }
}
