﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KYC.API.Entities
{
    public class Answer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AnswerId { get; set; }

        [Required]
        public int ChoiceId { get; set; }
        public Choice Choice { get; set; }
        
        [Required]
        public int UserProfileId { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }
    }
}