﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KYC.API.Entities
{
    public class Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionId { get; set; }

        [Required]
        [MaxLength(255)]
        [MinLength(3)]
        public string Text { get; set; }

        public ICollection<Choice> Choices { get; set; } = new HashSet<Choice>();

        [Required]
        public int CompanyId { get; set; }
        public Company Company { get; set; }

        [Required]
        public bool Visible { get; set; } = false;
    }
}