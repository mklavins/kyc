﻿using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KYC.API.Entities
{
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; } = new DateTime(2000, 1, 1);
        public bool IncludeDateOfBirth { get; set; } = true;
       
        [StringLength(20, MinimumLength = 3)]
        public string Phone { get; set; }

        public string GooglePlusProfileUrl { get; set; }
        public bool IncludeGooglePlusProfileUrl { get; set; } = true;

        [Required]
        public string ImageUrl { get; set; }

        public bool ProfileConfirmed { get; set; } = false;

        public bool IsCompanyAdmin { get; set; } = false;

        public Company Company { get; set; }
        public int CompanyId { get; set; }

        public ApplicationUser User { get; set; }
        [Required]
        public string UserId { get; set; }

        public ICollection<QuizAnswer> QuizAnswers { get; set; } = new HashSet<QuizAnswer>();
    }
}