﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KYC.API.Entities
{
    public class Choice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChoiceId { get; set; }

        [Required]
        [MaxLength(255)]
        [MinLength(3)]
        public string Text { get; set; }

        [Required]
        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public ICollection<Answer> Answers { get; set; } = new HashSet<Answer>();
    }
}