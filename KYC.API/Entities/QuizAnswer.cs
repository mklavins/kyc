﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KYC.API.Entities
{
    public class QuizAnswer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        public int ChoiceId { get; set; }

        public UserProfile User { get; set; }
        [Required]
        public int UserProfileId { get; set; }

        [Required]
        public int ColleagueProfileId { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public bool Correct { get; set; } = false;
    }
}