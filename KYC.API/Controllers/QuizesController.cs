﻿using AutoMapper;
using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KYC.API.Controllers
{
    [RoutePrefix("Quiz")]
    [Authorize]
    public class QuizController : ApiController
    {
        // gives access to repositories
        private IUnitOfWork unitOfWork;
        public QuizController(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        /// <summary>
        /// Returns randomly selected question and colleague, who answered it 
        /// See: S3.QUIZ.COLLEAGUE
        /// </summary>
        [HttpGet()]
        public IHttpActionResult GetRandomQuestion()
        {
            // Get random answer, that has been answered by colleague, not user himself
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            var randomAnswer = unitOfWork.Quiz.GetRandomAnswer(user.CompanyId, user.Id);

            if (randomAnswer == null)
                return NotFound();

            // Get colleague, who gave randomAnswer 
            var answerUser = unitOfWork.UserProfile.GetUser(randomAnswer.UserProfileId);
            
            var question = randomAnswer.Choice.Question;

            // Include necessary info for quiz in DTO
            var quizQuestionDto = new QuizQuestionDto()
            {
                UserId = answerUser.Id,
                UserName = answerUser.Name,
                UserImageUrl = answerUser.ImageUrl,
                QuestionText = question.Text,
                Choices = Mapper.Map<List<ChoiceDto>>(question.Choices.OrderBy(x => new Guid()))
            };

            return Ok(quizQuestionDto);
        }

        /// <summary>
        /// Returns quiz results for user- how many answers he has given and how many are correct.
        /// See: S3.QUIZ.COLLEAGUE, S4.USER.PROFILE.STATS
        /// </summary>
        [HttpGet()]
        [Route("Stats/User")]
        public IHttpActionResult GetUserQuizStatistics()
        {
            // get authenticated user total and correct answer count
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            var quizAnswerCount = unitOfWork.Quiz.GetTotalUserAnswerCount(user.Id);
            var quizCorrectAnswerCount = unitOfWork.Quiz.GetCorrectUserAnswerCount(user.Id);

            // Include necessary info for quiz in DTO
            var result = new QuizResultDto()
            {
                CorrectCount = quizCorrectAnswerCount,
                TotalCount = quizAnswerCount,
            };

            return Ok(result);
        }

        /// <summary>
        /// Returns quiz results about user colleagues - 
        /// how many answers they have given about user and how many of them are correct.
        /// See: S4.USER.PROFILE.STATS
        /// </summary>
        [HttpGet()]
        [Route("Stats/Colleagues")]
        public IHttpActionResult GetColleagueQuizStatistics()
        {
            // get authenticated user total and correct answer count
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            var quizAnswerCount = unitOfWork.Quiz.GetTotalColleagueAnswerCount(user.Id, user.CompanyId);
            var quizCorrectAnswerCount = unitOfWork.Quiz.GetCorrectColleagueAnswerCount(user.Id, user.CompanyId);

            // Include necessary info for quiz in DTO
            var result = new QuizResultDto()
            {
                CorrectCount = quizCorrectAnswerCount,
                TotalCount = quizAnswerCount,
            };

            return Ok(result);
        }

        /// <summary>
        /// The answer user has chosen in quiz, is checked, whether it is correct and saved in db.
        /// See: S3.QUIZ.COLLEAGUE
        /// </summary>
        [HttpPost()]
        public IHttpActionResult AnswerQuizQuestion([FromBody] QuizAnswerDto quizAnswerDto)
        {
            if (quizAnswerDto == null)
                return BadRequest();

            // Checks, if quiz answer includes choice id and colleague id
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Checks, whether chosen answer exists in db and whether answered question is visible
            var choice = unitOfWork.Quiz.GetChoice(quizAnswerDto.ChoiceId);
            if (choice == null || !choice.Question.Visible)
                return BadRequest();

            // Checks, whether the question belongs to user's company
            var companyId = unitOfWork.UserProfile.GetUserCompanyId(User.Identity.GetUserId());
            if (choice.Question.CompanyId != companyId)
                return StatusCode(HttpStatusCode.Forbidden);

            // Checks, if colleague exists and belongs to the same company
            var colleague = unitOfWork.UserProfile.GetUser(quizAnswerDto.ColleagueProfileId);
            if (colleague == null || colleague.CompanyId != companyId)
                return BadRequest();

            var quizAnswer = Mapper.Map<QuizAnswer>(quizAnswerDto);

            // Adds answerer to quiz answer and whether it is correct
            quizAnswer.User = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            var answerChoiceId = unitOfWork.Quiz.GetAnswer(colleague.Id, choice.QuestionId);
            quizAnswer.Correct = (answerChoiceId == quizAnswer.ChoiceId);

            // Saves quiz answer in db
            unitOfWork.Quiz.AnswerQuizQuestion(quizAnswer);
            unitOfWork.Save();
            
            return Ok(answerChoiceId);
        }
    }
}
