﻿using AutoMapper;
using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Interfaces;
using KYC.API.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KYC.API.Controllers
{
    /// <summary>
    /// Manages companies registered in system
    /// Some of functions can be accessed by users with Sysadmin role or, in some cases - company admin
    /// </summary>
    [Authorize]
    [RoutePrefix("Companies")]
    public class CompaniesController : ApiController
    {
        IUnitOfWork unitOfWork;

        public CompaniesController(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        /// <summary>
        /// Get all companies
        /// See S2.COMPANY.LIST
        /// </summary>
        [HttpGet()]
        [Authorize(Roles = RoleNames.SysAdmin)]
        public IHttpActionResult Index()
        {
            var companies = unitOfWork.Company.GetCompanies().ToList();

            // If there are no companies registered
            if (!companies.Any())
                return NotFound();

            var companiesDto = Mapper.Map<List<CompanyDto>>(companies);

            return Ok(companiesDto);
        }

        /// <summary>
        /// Get one particular company
        /// SEE S2.COMPANY.EDIT
        /// </summary>
        [HttpGet]
        [Route("{companyId:int}", Name = "GetCompany")]
        public IHttpActionResult GetCompany(int companyId)
        {
            // Only company admin or system admin can access this method
            var userId = User.Identity.GetUserId();
            if (!User.IsInRole(RoleNames.SysAdmin) && !unitOfWork.UserProfile.IsCompanyAdmin(userId, companyId))
                return StatusCode(HttpStatusCode.Forbidden);

            var company = unitOfWork.Company.GetCompany(companyId, true);

            if (company == null)
                return NotFound();

            var companyDto = Mapper.Map<Company, CompanyDto>(company);

            // Get all company admins
            companyDto.Admins = unitOfWork.UserProfile.GetUsers(companyId)
                                    .Where(u => u.IsCompanyAdmin && u.UserId != userId)
                                    .Select(u => u.UserId).ToArray();

            // Get all company users  
            companyDto.Users = companyDto.Users.Where(u => u.UserId != userId).ToList();

            return Ok(companyDto);
        }

        /// <summary>
        /// Create new company
        /// See S2.COMPANY.ADD
        /// </summary>
        [HttpPost]
        [Authorize(Roles = RoleNames.SysAdmin)]
        public IHttpActionResult Create([FromBody] CompanyDto companyDto)
        {
            if (companyDto == null)
                return BadRequest();

            // check whether company whith such name already exists
            if (unitOfWork.Company.CompanyExists(companyDto.Name))
                ModelState.AddModelError(string.Empty, "Company with such name already exists in the system!");

            // Whether all company data are valid
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            /// Whether there already is a company with the same e-mail domain
            if (unitOfWork.Company.CompanyForEmailExists(companyDto.Email) != 0)
            {
                ModelState.AddModelError(string.Empty, "Company with such e-mail host already exists in the system!");
                return BadRequest(ModelState);
            }

            var company = Mapper.Map<Company>(companyDto);

            // Add company to db
            unitOfWork.Company.AddCompany(company);
            unitOfWork.Save();

            // return newly created company
            var companyCreated = Mapper.Map<CompanyDto>(company);
            return Created(new Uri(Url.Link("GetCompany", new { companyId = company.CompanyId })), companyCreated);
        }

        /// <summary>
        /// Save edited company info
        /// See S2.COMPANY.EDIT
        /// </summary>
        [HttpPut]
        [Route("{companyId:int}")]
        public IHttpActionResult Put(int companyId, [FromBody] CompanyDto companyDto)
        {
            // Checks whether user is system or company admin
            var userId = User.Identity.GetUserId();
            if (!User.IsInRole(RoleNames.SysAdmin) && !unitOfWork.UserProfile.IsCompanyAdmin(userId, companyDto.CompanyId))
                return StatusCode(HttpStatusCode.Forbidden);

            if (companyDto == null)
                return BadRequest();
            
            // Get company by e-mail address
            var company = unitOfWork.Company.GetCompany(companyDto.Email);

            if (company == null)
            {
                ModelState.AddModelError(string.Empty, "Changing e-mail address host is not allowed!");
                return BadRequest(ModelState);
            }

            // If company, found by e-mail address is the same as edited company
            if (company.CompanyId != companyDto.CompanyId)
                ModelState.AddModelError(string.Empty, "Another company with such e-mail host already exists in the system!");

            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            Mapper.Map(companyDto, company);
            
            // Get company admin Ids
            var companyAdmins = unitOfWork.UserProfile.GetUsers(companyDto.CompanyId)
                                    .Where(u => u.IsCompanyAdmin)
                                    .Select(u => u.UserId).ToArray();

            // Remove from array admins, who are not included in Dto
            var notAdmins = companyAdmins.Except(companyDto.Admins);
            
            foreach (var notAdminId in notAdmins)
            {
                // Make sure, that user add or remove himself from company administrators
                if (notAdminId != userId)
                    unitOfWork.UserProfile.SetAdmin(notAdminId, false);
            }
            foreach (var adminId in companyDto.Admins)
            {
                // 
                if (unitOfWork.UserProfile.GetUserCompanyId(adminId) == company.CompanyId && adminId != userId)
                    unitOfWork.UserProfile.SetAdmin(adminId, true);
            }

            //Saves changes
            unitOfWork.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
