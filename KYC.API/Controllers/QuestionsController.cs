﻿using AutoMapper;
using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KYC.API.Controllers
{
    /// <summary>
    /// Manages questions and choices in system
    /// </summary>
    [RoutePrefix("Questions")]
    [Authorize]
    public class QuestionsController : ApiController
    {
        private IUnitOfWork unitOfWork;

        public QuestionsController(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        /// <summary>
        /// Return all company questions
        /// answers= false: add last answer, that user has left
        /// includeEmpty=true: include questions without choices
        /// companyId: returns user company questions- prohibits system admin to access other company questions
        /// Used in S3.QUIZ.SELF
        /// </summary>
        [HttpGet]
        [Route(Name ="GetQuestions")]
        public IHttpActionResult GetQuestions(bool answers = false, bool includeEmpty=true, int companyId=0)
        {
            // Whether user is company admin
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (companyId != 0 && user.CompanyId != companyId)
                return StatusCode(HttpStatusCode.Forbidden);                

            // Depending on whether choices are needed, get data from db
            List<Question> questions = (includeEmpty && user.IsCompanyAdmin)
                ? questions = unitOfWork.Company.GetQuestions(user.CompanyId, true).ToList()
                : questions = unitOfWork.Company.GetQuestions(user.CompanyId, true).Where(q => q.Choices.Any() && q.Visible).ToList();
            
            // If there are no questions
            if (!questions.Any())
                return StatusCode(HttpStatusCode.NoContent);
            
            // If answers, that user left, has to include for every answered question
            if (answers)
            {
                var questionsDto = Mapper.Map<List<QuestionWithAnswerDto>>(questions);
                // Get current user answers to questions
                //var userAnswers = unitOfWork.UserProfile.GetAnswers(user.Id).ToList();
                //// Match answer with question
                //foreach (var userAnswer in userAnswers)
                //    questionsDto.Where(q => q.QuestionId == userAnswer.Choice.QuestionId).FirstOrDefault().AnswerId = userAnswer.ChoiceId;

                foreach (var question in questionsDto)
                    question.AnswerId = unitOfWork.Quiz.GetAnswer(user.Id, question.QuestionId);

                return Ok(questionsDto);
            }
            else
            {
                var questionsDto = Mapper.Map<List<QuestionDto>>(questions);
                return Ok(questionsDto);
            }
        }

        /// <summary>
        /// Get particular question
        /// </summary>
        [HttpGet]
        [Route("{questionId:int}", Name = "GetQuestion")]
        public IHttpActionResult GetQuestion(int questionId, bool choices = false)
        {
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            var question = unitOfWork.Company.GetQuestion(questionId, choices);

            if (question == null)
                return NotFound();

            var questionResult = Mapper.Map<QuestionDto>(question);

            return Ok(questionResult);
        }

        /// <summary>
        /// Get particular choice
        /// </summary>
        [HttpGet]
        [Route("{questionId:int}/Choices/{choiceId:int}", Name = "GetChoice")]
        public IHttpActionResult GetChoice(int questionId, int choiceId)
        {
            // TODO
            return Ok();
        }

        /// <summary>
        /// Create new question for company
        /// See: S3.COMPANY.QUESTION.ADD
        /// </summary>
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateQuestion([FromBody]QuestionAddDto questionDto)
        {
            if (questionDto == null)
                return BadRequest();

            // Whether user is company admin
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (!user.IsCompanyAdmin)
                return StatusCode(HttpStatusCode.Forbidden);

            var company = unitOfWork.Company.GetCompany(user.CompanyId);
            if (company == null)
                return BadRequest();

            // Whether question text conform length restrictions
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var question = Mapper.Map<Question>(questionDto);

            // Add question to db
            unitOfWork.Company.AddQuestion(company, question);
            unitOfWork.Save();

            // Return created question to user
            var createdQuestion = Mapper.Map<QuestionDto>(question);
            return CreatedAtRoute("GetQuestion",
                new
                {
                    questionId = createdQuestion.QuestionId
                },
                createdQuestion);
        }

        /// <summary>
        /// Set question visibility
        /// See S4.COMPANY.QUESTION.VISIBLE
        /// </summary>
        [HttpPost]
        [Route("{questionId:int}")]
        public IHttpActionResult ChangeQuestionVisibility(int questionId, bool visible)
        {
            // Whether user is company admin
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (!user.IsCompanyAdmin)
                return StatusCode(HttpStatusCode.Forbidden);

            // Save new visibility in db
            unitOfWork.Company.SetQuestionVisibility(questionId, visible);
            unitOfWork.Save();

            return Ok();
        }

        /// <summary>
        /// Remove question from db
        /// See S4.COMPANY.QUESTION.DELETE
        /// </summary>
        [HttpDelete]
        [Route("{questionId:int}")]
        public IHttpActionResult DeleteQuestion(int questionId)
        {
            // whether user is company admin
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (!user.IsCompanyAdmin)
                return StatusCode(HttpStatusCode.Forbidden);

            var question = unitOfWork.Company.GetQuestion(questionId);
            if (question == null)
                return BadRequest();

            // Make changes in db
            unitOfWork.Company.DeleteQuestion(question);
            unitOfWork.Save();

            return Ok();
        }

        /// <summary>
        /// Create new choice for question
        /// See S3.COMPANY.CHOICE.ADD
        /// </summary>
        [HttpPost]
        [Route("{questionId:int}/Choices")]
        public IHttpActionResult CreateChoice(int questionId, [FromBody]ChoiceDto choiceDto)
        {
            // Whether user is company admin
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (!user.IsCompanyAdmin)
                return StatusCode(HttpStatusCode.Forbidden);

            if (choiceDto == null)
                return BadRequest();

            // Whether choice text conform length restrictions
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var question = unitOfWork.Company.GetQuestion(questionId, true);
            // If question exists in db and belongs to company in which user is an admin
            if (question == null || question.CompanyId != user.CompanyId)
                return BadRequest();

            // Save choice in db
            var choice = Mapper.Map<Choice>(choiceDto);
            unitOfWork.Company.AddChoice(question, choice);
            unitOfWork.Save();

            // Return newly created choice
            var createdChoice = Mapper.Map<ChoiceDto>(choice);
            return CreatedAtRoute("GetChoice",
                new
                {
                    questionId = questionId,
                    choiceId = createdChoice.ChoiceId
                },
                createdChoice);
        }

        /// <summary>
        /// Save answer made by user in his questionnaire
        /// See S3.QUIZ.SELF
        /// </summary>
        [HttpPost]
        [Route("{questionId:int}")]
        public IHttpActionResult AnswerQuestion(int questionId, [FromBody]AnswerDto answerDto)
        {
            if (answerDto == null)
                return BadRequest();

            // Whether question exists in db and is visible
            var question = unitOfWork.Company.GetQuestion(questionId, true);
            if (question == null || !question.Visible)
                return BadRequest();

            // Whether user belongs to same company as question
            var user = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());
            if (user.CompanyId != question.CompanyId)
                return StatusCode(HttpStatusCode.Forbidden);

            // Whether choice belongs to the question
            var choice = question.Choices.Where(c => c.ChoiceId == answerDto.ChoiceId).FirstOrDefault();
            if (choice == null)
                return BadRequest();

            // Save answer in db
            unitOfWork.Company.SaveAnswer(choice, user.Id);
            unitOfWork.Save();

            return Ok();
        }
    }
}
