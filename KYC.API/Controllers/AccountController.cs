﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using KYC.API.Models;
using KYC.API.Providers;
using KYC.API.Results;
using KYC.API.Repositories;
using KYC.API.Entities;
using System.Linq;
using KYC.API.Dtos;
using AutoMapper;
using KYC.API.Interfaces;
using System.Net;

namespace KYC.API.Controllers
{
    /// <summary>
    /// Functions related to user authentication, registration and profile management
    /// </summary>
    [Authorize]
    [RoutePrefix("Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private IUnitOfWork unitOfWork;

        public AccountController(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat,
            IUnitOfWork _unitOfWork)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
            unitOfWork = _unitOfWork;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        /// <summary>
        /// Gets user info. Used to find out, whether user has been registered in the system
        /// See S1.USER.AUTHENTICATE
        /// </summary>
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            bool hasRegistered = externalLogin == null;
            string userName = (hasRegistered) ? unitOfWork.UserProfile.GetUserName(User.Identity.GetUserId()) : externalLogin.UserName;
            bool isAdmin = User.IsInRole(RoleNames.SysAdmin);

            return new UserInfoViewModel
            {
                Name = userName,
                Email = User.Identity.GetUserName(),
                IsAdmin = isAdmin,
                HasRegistered = hasRegistered,
                LoginProvider = !hasRegistered ? externalLogin.LoginProvider : null
            };
        }
        /// <summary>
        /// Authenticates user in system through Google
        /// See S1.USER.AUTHENTICATE
        /// </summary>
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        {
            if (error != null)
            {
                return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new ChallengeResult(provider, this);
            }

            ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
                externalLogin.ProviderKey));

            bool hasRegistered = user != null;

            if (hasRegistered)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

                ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
                   OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
                    CookieAuthenticationDefaults.AuthenticationType);

                AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
                Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);

                // Check, whether user info from Google is the same as info in db
                // Get info sent by Google
                var info = await Authentication.GetExternalLoginInfoAsync();
                // Get info from db
                var userProfile = unitOfWork.UserProfile.GetUser(user.Id);
                
                if (info != null || userProfile != null)
                {
                    // extract necessary Google info
                    string name = info.ExternalIdentity.Name;
                    string imageUrl = info.ExternalIdentity.FindFirstValue("imageUrl");
                    string profileUrl = info.ExternalIdentity.FindFirstValue("urn:google:profile");

                    // check for changes and, if necessary, update profile
                    if (name != userProfile.Name)
                        unitOfWork.UserProfile.UpdateName(userProfile, name);
                    if (imageUrl != userProfile.ImageUrl)
                        unitOfWork.UserProfile.UpdateImageUrl(userProfile, imageUrl);
                    if (profileUrl != userProfile.GooglePlusProfileUrl)
                        unitOfWork.UserProfile.UpdateGooglePlusProfileurl(userProfile, profileUrl);

                    // save profile changes in db
                    unitOfWork.Save();
                }
            }
            else
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                Authentication.SignIn(identity);
            }

            return Ok();
        }

        // POST api/Account/RegisterExternal
        ///<summary>
        /// Registers user in system. Creates new User and UserProfile and adds them to db.
        /// See S1.USER.AUTHENTICATE
        /// </summary>
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("RegisterExternal")]
        public async Task<IHttpActionResult> RegisterExternal()
        {
            var info = await Authentication.GetExternalLoginInfoAsync();
            
            if (info == null)
            {
                return InternalServerError();
            }

            // Checks, if there is a company for user to be added in. User e-mail host needs to be the same as for company e-mail address.
            var companyId = unitOfWork.Company.CompanyForEmailExists(info.Email);
            if (companyId == 0)
            {
                Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                // If there is no company, user is not allowed to register.
                return Content(HttpStatusCode.NotFound, "Company for user does not exist! Authentication terminated!");
            }
            
            var user = new ApplicationUser() {
                UserName = info.Email, //User.Identity.GetUserName(), - user name should be unique in system- cannot save user real name here
                Email = info.Email
            };

            // Saves user profile as AspNetUser
            IdentityResult result = await UserManager.CreateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            result = await UserManager.AddLoginAsync(user.Id, info.Login);
            if (!result.Succeeded)
            {
                return GetErrorResult(result); 
            }

            // Creates user profile. Google API claims are used as data source.
            unitOfWork.UserProfile.Add(new UserProfile()
            {
                UserId = user.Id,
                Name = info.ExternalIdentity.Name,
                ImageUrl = info.ExternalIdentity.FindFirstValue("imageUrl"),
                GooglePlusProfileUrl = info.ExternalIdentity.FindFirstValue("urn:google:profile"),
                CompanyId = companyId,
                //Language = info.ExternalIdentity.FindFirstValue("language")
            });

            unitOfWork.Save();
            return Ok();
        }

        // GET api/Account/GetProfileStatus
        /// <summary>
        /// Gets user profile status messages.
        /// * is user profile information accepted after registration
        /// * has he answered to at least one question in questionnaire
        /// </summary>
        [HttpGet]
        [Route("Status")]
        public IHttpActionResult GetProfileStatus()
        {
            var userId = User.Identity.GetUserId();

            UserProfileStatusDto profileStatus = unitOfWork.UserProfile.GetProfileStatus(userId);

            return Ok(profileStatus);
        }

        //GET api/Account/Edit
        /// <summary>
        /// Returns user profile information to be edited by himself.
        /// See S1.USER.PROFILE.EDIT
        /// </summary>
        [HttpGet]
        [Route("Edit")]
        public IHttpActionResult EditUserProfile()
        {
            // Gets user profile about himself
            var userProfile = unitOfWork.UserProfile.GetUser(User.Identity.GetUserId());

            if (userProfile == null)
                return NotFound();

            var userProfileDto = Mapper.Map<UserProfile, UserProfileEditDto>(userProfile);

            // Dto dateOfBirth is in string format- there needs to be conversion between DateTime and String in the correct format.
            var dateofBirthString = userProfile.DateOfBirth.ToString("yyyy/MM/dd");
            userProfileDto.DateOfBirth = dateofBirthString;

            // Adds user email address from AspNetUsers entity
            userProfileDto.Email = User.Identity.GetUserName();

            return Ok(userProfileDto);
        }

        //POST api/Account/Edit
        /// <summary>
        /// Saves changes made by user in his user profile
        /// See S1.USER.PROFILE.EDIT
        /// </summary>
        [HttpPut]
        [Route("Edit")]
        public IHttpActionResult PutUserProfile([FromBody]UserProfileUpdateDto userProfileDto)
        {
            // Checks if selected date is valid- it is between 100 years ago and now
            if (userProfileDto.DateOfBirth < DateTime.Now.AddYears(-100) ||
                userProfileDto.DateOfBirth > DateTime.Now)
            {
                ModelState.AddModelError(string.Empty, "Selected date of birth is not valid date! Please try again.");
            }

            // Checks if phone number is valid (length between 3 and 20 symbols)
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userProfile = Mapper.Map<UserProfile>(userProfileDto);

            // Update and saves changes made by user
            unitOfWork.UserProfile.Update(User.Identity.GetUserId(), userProfile);
            unitOfWork.Save();

            var userProfileUpdated = Mapper.Map<UserProfileEditDto>(userProfile);

            return Content(HttpStatusCode.NoContent, userProfileUpdated);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
