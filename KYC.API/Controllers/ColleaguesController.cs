﻿using AutoMapper;
using KYC.API.Dtos;
using KYC.API.Entities;
using KYC.API.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace KYC.API.Controllers
{
    /// <summary>
    /// Manages requests regarding user colleagues
    /// </summary>
    [RoutePrefix("Colleagues")]
    public class ColleaguesController : ApiController
    {
        private IUnitOfWork unitOfWork;

        public ColleaguesController(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
        }
        
        /// <summary>
        /// Gets list of users from the same company as uer - colleagues
        /// See S1.USER.LIST
        /// </summary>
        [HttpGet]
        public IHttpActionResult Index()
        {
            // Gets list of users with the same company user is registered in
            // List excludes user himself and unconfirmed profiles
            var userId = User.Identity.GetUserId();
            var userCompanyId = unitOfWork.UserProfile.GetUserCompanyId(userId);
            List<UserProfile> colleagues = unitOfWork.UserProfile.GetUsers(userCompanyId)
                                            .Where(u => u.UserId != userId
                                            &&  u.ProfileConfirmed == true).ToList();

            // if there are no colleagues
            if (colleagues == null)
            {
                return NotFound();
            }

            // Removes all the info, that colleagues doesn't want to see
            foreach (var colleague in colleagues)
            {
                if (!colleague.IncludeDateOfBirth)
                    colleague.DateOfBirth = DateTime.MinValue;
                if (!colleague.IncludeGooglePlusProfileUrl)
                    colleague.GooglePlusProfileUrl = null;
            }

            List<UserProfileViewDto> colleaguesDto =
                Mapper.Map<List<UserProfile>, List<UserProfileViewDto>>(colleagues);

            // Add email addresses for each user from AspNetUsers db table
            foreach (var colleague in colleaguesDto)
            {
                colleague.Email = unitOfWork.UserProfile.GetUserEmail(colleague.UserId);
            }

            return Ok(colleaguesDto);
        }

        /// <summary>
        /// Returns info about user.
        /// See S1.USER.PROFILE.VIEW
        /// </summary>
        /// <param name="userId">Id of a selected user.</param>
        /// <returns>User profile view data transfer object</returns>
        [Route("{userId}", Name = "GetUserProfile")]
        public IHttpActionResult GetUserProfile(string userId)
        {
            var userProfile = unitOfWork.UserProfile.GetUser(userId);

            // Checks if selected user exists in system
            if (userProfile == null)
                return NotFound();

            // Checks if selected user belongs to the same company as user, who is requesting the info
            else if (userId != User.Identity.GetUserId())
            {
                if (userProfile.CompanyId != unitOfWork.UserProfile.GetUserCompanyId(User.Identity.GetUserId()))
                    return Unauthorized();
            }

            var dateofBirthString = userProfile.DateOfBirth.ToString("yyyy/MM/dd");

            var userProfileDto = Mapper.Map<UserProfile, UserProfileViewDto>(userProfile);

            // Removes date of birth if user doesn't want to show it in his profile
            userProfileDto.DateOfBirth = (userProfile.IncludeDateOfBirth)
                ? dateofBirthString
                : null;

            // Removes Google+ profile link if user doesn't want to show it in his profile
            if (!userProfile.IncludeGooglePlusProfileUrl)
            {
                userProfileDto.GooglePlusProfileUrl = null;
            }

            // Adds user email address from AspNetUsers entity

            userProfileDto.Email = unitOfWork.UserProfile.GetUserEmail(userId);

            return Ok(userProfileDto);
        }
    }
}
