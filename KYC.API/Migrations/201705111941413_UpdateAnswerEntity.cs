namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateAnswerEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "UserProfileId", c => c.Int(nullable: false));
            AddColumn("dbo.QuizAnswers", "ColleagueProfileId", c => c.Int(nullable: false));
            DropColumn("dbo.Answers", "UserId");
            DropColumn("dbo.QuizAnswers", "ColleagueId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QuizAnswers", "ColleagueId", c => c.Int(nullable: false));
            AddColumn("dbo.Answers", "UserId", c => c.String(nullable: false));
            DropColumn("dbo.QuizAnswers", "ColleagueProfileId");
            DropColumn("dbo.Answers", "UserProfileId");
        }
    }
}
