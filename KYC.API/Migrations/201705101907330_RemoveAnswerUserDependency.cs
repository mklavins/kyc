namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAnswerUserDependency : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Answers", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Answers", new[] { "UserId" });
            AlterColumn("dbo.Answers", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Answers", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Answers", "UserId");
            AddForeignKey("dbo.Answers", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
