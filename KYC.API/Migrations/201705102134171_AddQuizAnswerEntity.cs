namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuizAnswerEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QuizAnswers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoiceId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                        ColleagueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuizAnswers", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.QuizAnswers", new[] { "UserProfileId" });
            DropTable("dbo.QuizAnswers");
        }
    }
}
