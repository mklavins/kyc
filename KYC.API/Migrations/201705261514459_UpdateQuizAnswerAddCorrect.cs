namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateQuizAnswerAddCorrect : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuizAnswers", "Correct", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuizAnswers", "Correct");
        }
    }
}
