namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveAdminFromCompanyAddToUserProfileAsBool : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ApplicationUserCompanies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ApplicationUserCompanies", "Company_CompanyId", "dbo.Companies");
            DropIndex("dbo.ApplicationUserCompanies", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationUserCompanies", new[] { "Company_CompanyId" });
            AddColumn("dbo.UserProfiles", "IsCompanyAdmin", c => c.Boolean(nullable: false));
            DropTable("dbo.ApplicationUserCompanies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ApplicationUserCompanies",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Company_CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Company_CompanyId });
            
            DropColumn("dbo.UserProfiles", "IsCompanyAdmin");
            CreateIndex("dbo.ApplicationUserCompanies", "Company_CompanyId");
            CreateIndex("dbo.ApplicationUserCompanies", "ApplicationUser_Id");
            AddForeignKey("dbo.ApplicationUserCompanies", "Company_CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUserCompanies", "ApplicationUser_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
