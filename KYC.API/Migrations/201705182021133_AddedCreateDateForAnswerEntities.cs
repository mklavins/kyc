namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCreateDateForAnswerEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.QuizAnswers", "DateCreated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuizAnswers", "DateCreated");
            DropColumn("dbo.Answers", "DateCreated");
        }
    }
}
