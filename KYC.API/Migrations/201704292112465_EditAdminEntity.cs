namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditAdminEntity : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Admins", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Admins", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.Admins", new[] { "CompanyId" });
            DropIndex("dbo.Admins", new[] { "UserProfileId" });
            CreateTable(
                "dbo.ApplicationUserCompanies",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Company_CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Company_CompanyId })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Companies", t => t.Company_CompanyId, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Company_CompanyId);
            
            DropTable("dbo.Admins");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.ApplicationUserCompanies", "Company_CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ApplicationUserCompanies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ApplicationUserCompanies", new[] { "Company_CompanyId" });
            DropIndex("dbo.ApplicationUserCompanies", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ApplicationUserCompanies");
            CreateIndex("dbo.Admins", "UserProfileId");
            CreateIndex("dbo.Admins", "CompanyId");
            AddForeignKey("dbo.Admins", "UserProfileId", "dbo.UserProfiles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Admins", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
        }
    }
}
