namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUserProfileEntityAddConfirming : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserProfiles", new[] { "UserId" });
            AddColumn("dbo.UserProfiles", "IncludeDateOfBirth", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfiles", "IncludeGooglePlusProfileUrl", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfiles", "ProfileConfirmed", c => c.Boolean(nullable: false));
            AlterColumn("dbo.UserProfiles", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.UserProfiles", "UserId");
            AddForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.UserProfiles", "Language");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "Language", c => c.String());
            DropForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserProfiles", new[] { "UserId" });
            AlterColumn("dbo.UserProfiles", "UserId", c => c.String(maxLength: 128));
            DropColumn("dbo.UserProfiles", "ProfileConfirmed");
            DropColumn("dbo.UserProfiles", "IncludeGooglePlusProfileUrl");
            DropColumn("dbo.UserProfiles", "IncludeDateOfBirth");
            CreateIndex("dbo.UserProfiles", "UserId");
            AddForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
