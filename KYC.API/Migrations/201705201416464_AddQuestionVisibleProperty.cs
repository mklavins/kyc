namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQuestionVisibleProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "Visible", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Questions", "Visible");
        }
    }
}
