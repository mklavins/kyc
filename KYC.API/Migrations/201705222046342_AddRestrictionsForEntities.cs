namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRestrictionsForEntities : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Companies", "Address", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Companies", "Email", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Companies", "Phone", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.UserProfiles", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.UserProfiles", "Phone", c => c.String(maxLength: 20));
            AlterColumn("dbo.UserProfiles", "ImageUrl", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserProfiles", "ImageUrl", c => c.String());
            AlterColumn("dbo.UserProfiles", "Phone", c => c.String());
            AlterColumn("dbo.UserProfiles", "Name", c => c.String());
            AlterColumn("dbo.Companies", "Phone", c => c.String(maxLength: 20));
            AlterColumn("dbo.Companies", "Email", c => c.String(maxLength: 255));
            AlterColumn("dbo.Companies", "Address", c => c.String(maxLength: 255));
        }
    }
}
