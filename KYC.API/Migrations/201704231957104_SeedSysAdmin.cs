namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedSysAdmin : DbMigration
    {
        public override void Up()
        {
            Sql(@"
            SET IDENTITY_INSERT [dbo].[Companies] ON 
            INSERT [dbo].[Companies] ([CompanyId], [Name], [Address], [Email], [Phone]) VALUES (1, N'KYC Inc', N'Riga, Latvia', N'kyc@knowyourcolleague.com', N'123')
            SET IDENTITY_INSERT [dbo].[Companies] OFF
            INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1e0db9bc-c4c4-48a6-b8db-d77eb4436151', N'user.kyc@gmail.com', 0, NULL, N'49772298-9aa7-4474-8b4c-a281e1b8014f', NULL, 0, 0, NULL, 0, 0, N'user.kyc@knowyourcolleague.com')
            SET IDENTITY_INSERT [dbo].[UserProfiles] ON 

            INSERT [dbo].[UserProfiles] ([Id], [Name], [DateOfBirth], [Phone], [GooglePlusProfileUrl], [ImageUrl], [CompanyId], [UserId], [IncludeDateOfBirth], [IncludeGooglePlusProfileUrl], [ProfileConfirmed]) VALUES (1, N'Test User', CAST(N'2000-01-01 00:00:00.000' AS DateTime), N'123', NULL, N'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 1, N'1e0db9bc-c4c4-48a6-b8db-d77eb4436151', 1, 0, 1)
            SET IDENTITY_INSERT [dbo].[UserProfiles] OFF
            INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'0e899208-4361-42d3-877d-19bd6369a29c', N'SysAdmin')
            INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1e0db9bc-c4c4-48a6-b8db-d77eb4436151', N'0e899208-4361-42d3-877d-19bd6369a29c')
            INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Google', N'102290805413789928140', N'1e0db9bc-c4c4-48a6-b8db-d77eb4436151')
            ");
        }
        
        public override void Down()
        {
        }
    }
}
