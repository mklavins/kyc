namespace KYC.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserProfileAndAdminEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        UserProfileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId, cascadeDelete: false)
                .Index(t => t.CompanyId)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Language = c.String(),
                        Phone = c.String(),
                        GooglePlusProfileUrl = c.String(),
                        ImageUrl = c.String(),
                        CompanyId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.CompanyId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfiles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserProfiles", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Admins", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.Admins", "CompanyId", "dbo.Companies");
            DropIndex("dbo.UserProfiles", new[] { "UserId" });
            DropIndex("dbo.UserProfiles", new[] { "CompanyId" });
            DropIndex("dbo.Admins", new[] { "UserProfileId" });
            DropIndex("dbo.Admins", new[] { "CompanyId" });
            DropTable("dbo.UserProfiles");
            DropTable("dbo.Admins");
        }
    }
}
