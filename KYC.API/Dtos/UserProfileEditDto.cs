﻿using KYC.API.Entities;
using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KYC.API.Dtos
{
    public class UserProfileEditDto
    {
        public string Name { get; set; }

        public string DateOfBirth { get; set; }
        public bool IncludeDateOfBirth { get; set; }

        public string Phone { get; set; }

        public string GooglePlusProfileUrl { get; set; }
        public bool IncludeGooglePlusProfileUrl { get; set; }

        public string ImageUrl { get; set; }

        public bool ProfileConfirmed { get; set; }

        public string Email { get; set; }

        public int CompanyId { get; set; }

        public bool IsCompanyAdmin { get; set; }
    }

}