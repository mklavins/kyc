﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class QuizResultDto
    {
        public int CorrectCount { get; set; }
        public int TotalCount { get; set; }
    }
}