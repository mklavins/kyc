﻿using KYC.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class QuestionDto
    {
        public int QuestionId { get; set; }

        public string Text { get; set; }

        public bool Visible { get; set; }

        public ICollection<ChoiceDto> Choices { get; set; } = new HashSet<ChoiceDto>();
    }
}