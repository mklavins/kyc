﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;

namespace KYC.API.Dtos
{
    public class CompanyDto
    {
        private string name;
        private string address;
        private string phone;

        public int CompanyId { get; set; }

        [MinLength(3, ErrorMessage = "Name must be at least {1} characters long.")]
        [MaxLength(50, ErrorMessage = "Name must be no longer than {1} characters.")]
        public string Name {
            get { return name; }
            set { name = AntiXssEncoder.HtmlEncode(value, false); }
        }

        [MinLength(3, ErrorMessage = "Address must be at least {1} characters long.")]
        [MaxLength(255, ErrorMessage = "Address must be no longer than {1} characters.")]
        public string Address
        {
            get { return address; }
            set { address = AntiXssEncoder.HtmlEncode(value, false); }
        }

        [Required]
        [EmailAddress(ErrorMessage = "E-mail address is not valid!")]
        public string Email { get; set; }

        [Display(Name = "Phone number")]
        [MinLength(3, ErrorMessage = "{0} must be at least {1} characters long.")]
        [MaxLength(20, ErrorMessage = "{0} must be no longer than {1} characters.")]

        public string Phone
        {
            get { return phone; }
            set { phone = AntiXssEncoder.HtmlEncode(value, false); }
        }

        public IEnumerable<UserDto> Users { get; set; }
        public string[] Admins { get; set; }
    }
}