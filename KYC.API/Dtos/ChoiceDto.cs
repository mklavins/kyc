﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Security.AntiXss;

namespace KYC.API.Dtos
{
    public class ChoiceDto
    {
        private string text;

        public int ChoiceId { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "Choice text must be at least {1} characters long.")]
        [MaxLength(255, ErrorMessage = "Choice text must be no longer than {1} characters.")]
        public string Text
        {
            get { return text; }
            set { text = AntiXssEncoder.HtmlEncode(value, false); }
        }

        [Required]
        public int QuestionId { get; set; }
    }
}