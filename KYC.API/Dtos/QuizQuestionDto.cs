﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class QuizQuestionDto
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserImageUrl { get; set; }
        public string QuestionText { get; set; }
        public ICollection<ChoiceDto> Choices { get; set; }
    }
}