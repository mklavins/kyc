﻿using KYC.API.Entities;
using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Security.AntiXss;

namespace KYC.API.Dtos
{
    public class UserProfileUpdateDto
    {
        private string phone;

        public DateTime DateOfBirth;
        public bool IncludeDateOfBirth { get; set; }

        [Display(Name = "Phone number")]
        [MinLength(3, ErrorMessage = "{0} must be at least {1} characters long.")]
        [MaxLength(20, ErrorMessage = "{0} must be no longer than {1} characters.")]
        public string Phone
        {
            get { return phone; }
            set { phone = AntiXssEncoder.HtmlEncode(value, false); }
        }

        public bool IncludeGooglePlusProfileUrl { get; set; }

        public bool ProfileConfirmed { get; set; }
    }
}