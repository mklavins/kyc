﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class UserProfileStatusDto
    {
        public bool ProfileConfirmed { get; set; }
        public bool QuestionsAnswered { get; set; }
    }
}