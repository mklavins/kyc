﻿using KYC.API.Entities;
using KYC.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KYC.API.Dtos
{
    public class UserProfileViewDto
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string DateOfBirth { get; set; }

        public string Phone { get; set; }

        public string GooglePlusProfileUrl { get; set; }

        public string ImageUrl { get; set; }

        public string Email { get; set; }

        public bool IsCompanyAdmin { get; set; }
    }
}