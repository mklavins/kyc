﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class QuizAnswerDto
    {
        public int ChoiceId { get; set; }
        public int ColleagueProfileId { get; set; }
    }
}