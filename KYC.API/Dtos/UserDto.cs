﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KYC.API.Dtos
{
    public class UserDto
    {
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}