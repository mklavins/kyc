﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using KYC.API.Providers;
using KYC.API.Models;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Configuration;

namespace KYC.API
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Enable Google authentication
            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = ConfigurationManager.AppSettings["ClientId"],
                ClientSecret = ConfigurationManager.AppSettings["ClientSecret"],
                Provider = new CustomGoogleProvider()
            });
        }
    }

    public class CustomGoogleProvider : GoogleOAuth2AuthenticationProvider
    {
        // Sets additional information from Google+ profile to be available at registration
        public override Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            var identity = ((ClaimsIdentity)context.Identity);

            // User name [, middle name,] and surname
            //if (context.User["displayName"] != null)
            //{
            //    var name = context.User["displayName"].ToString();
            //    identity.AddClaim(new Claim("displayName", name));
            //}

            // User Google+ profile image link
            if (context.User["image"] != null)
            {
                var imageUrl = context.User["image"].Value<string>("url");
                imageUrl = imageUrl.Remove(imageUrl.IndexOf('?'), 6);
                identity.AddClaim(new Claim("imageUrl", imageUrl));
            }

            // User Google+ language preference
            //if (context.User["language"] != null)
            //{
            //    var language = context.User["language"].ToString();
            //    identity.AddClaim(new Claim("language", language));
            //}

            // User Google+ account link
            //if (context.User["url"] != null)
            //{
            //    var profileUrl = context.User["url"].ToString();
            //    identity.AddClaim(new Claim("url", profileUrl));
            //}

            return base.Authenticated(context);
        }
    }
}
