﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using KYC.API.Entities;
using KYC.API.Dtos;

namespace KYC.API.App_Start
{
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Creates mapping profiles for data transfer objects
        /// </summary>
        public MappingProfile()
        {
            Mapper.CreateMap<UserProfile, UserProfileViewDto>();
            Mapper.CreateMap<UserProfile, UserProfileEditDto>();
            Mapper.CreateMap<UserProfileUpdateDto, UserProfile>();
            Mapper.CreateMap<UserProfile, UserDto>();

            // used for user profile updating
            Mapper.CreateMap<UserProfile, UserProfile>()
                .ForMember(dest => dest.CompanyId, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ImageUrl, opt => opt.Ignore())
                .ForMember(dest => dest.GooglePlusProfileUrl, opt => opt.Ignore())
                .ForMember(dest => dest.IsCompanyAdmin, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.Ignore());

            Mapper.CreateMap<Company, CompanyDto>()
                .ForMember(d => d.Users, m => m.MapFrom(s => s.Users));
            Mapper.CreateMap<CompanyDto, Company>()
                .ForMember(dest => dest.CompanyId, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore());

            Mapper.CreateMap<Choice, ChoiceDto>();
            Mapper.CreateMap<ChoiceDto, Choice>();

            Mapper.CreateMap<QuestionAddDto, Question>();
            Mapper.CreateMap<Question, QuestionWithAnswerDto>();
            // Map not only question, but also its choices
            Mapper.CreateMap<Question, QuestionDto>()
                .ForMember(d => d.Choices, m => m.MapFrom(s => s.Choices));

            Mapper.CreateMap<QuizAnswerDto, QuizAnswer>();
        }
    }
}